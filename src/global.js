import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    body {
      margin: 0; line-height: normal;
    }
:root {

/* fonts */
--medium-16: 'Noto Sans KR';

/* font sizes */
--medium-16-size: 1rem;
--font-size-xl: 1.25rem;
--font-size-xs: 0.75rem;
--font-size-13xl: 2rem;
--font-size-7xl: 1.625rem;
--font-size-lgi: 1.188rem;
--font-size-11xl: 1.875rem;
--font-size-5xl: 1.5rem;
--font-size-lg: 1.125rem;
--medium-14-size: 0.875rem;
--font-size-mini: 0.938rem;

/* Colors */
--gray-f7f8f9: #f7f8f9;
--gray-f1f1f1: #f1f1f1;
--gray-e8ebed: #e8ebed;
--white-ffffff: #fff;
--gray-c9cdce: #c9cdce;
--gray-72787f: #72787f;
--gray-26282b: #26282b;
--text-primary: #111827;
--color-gray-100: rgba(114, 120, 127, 0.85);
--main-0abeba: #0abeba;
--color-dimgray: #595c5e;

/* Gaps */
--gap-3xs: 0.625rem;
--gap-lg: 1.125rem;
--gap-4xl: 1.437rem;
--gap-21xl: 2.5rem;
--gap-lgi: 1.187rem;
--gap-xl: 1.25rem;
--gap-6xs: 0.437rem;
--gap-5xs: 0.5rem;
--gap-45xl: 4rem;
--gap-base: 1rem;
--gap-13xl: 2rem;
--gap-sm: 0.875rem;
--gap-6xl: 1.562rem;
--gap-xs: 0.75rem;
--gap-20xl-6: 2.475rem;
--gap-34xl: 3.312rem;
--gap-7xl: 1.625rem;
--gap-28xl: 2.937rem;
--gap-2xl: 1.312rem;
--gap-9xs: 0.25rem;
--gap-9xl: 1.75rem;
--gap-mini-8: 0.925rem;
--gap-2xs: 0.687rem;
--gap-11xs: 0.125rem;
--gap-mini: 0.937rem;
--gap-8xl: 1.687rem;
--gap-35xl: 3.375rem;
--gap-20xl: 2.437rem;
--gap-7xl-5: 1.656rem;
--gap-18xl: 2.312rem;
--gap-3xl: 1.375rem;
--gap-4xs: 0.562rem;
--gap-smi: 0.812rem;
--gap-89xl: 6.75rem;
--gap-16xl: 2.187rem;
--gap-80xl: 6.187rem;
--gap-30xl: 3.062rem;
--gap-73xl: 5.75rem;
--gap-27xl: 2.875rem;
--gap-85xl: 6.5rem;
--gap-33xl: 3.25rem;
--gap-25xl: 2.75rem;

/* Paddings */
--padding-10xs: 0.187rem;
--padding-3xs: 0.625rem;
--padding-27xl: 2.875rem;
--padding-xl: 1.25rem;
--padding-11xs: 0.125rem;
--padding-xs: 0.75rem;
--padding-9xl: 1.75rem;
--padding-5xl: 1.5rem;
--padding-lg: 1.125rem;
--padding-lgi: 1.187rem;
--padding-4xl-5: 1.468rem;
--padding-sm-5: 0.843rem;
--padding-base: 1rem;
--padding-5xs: 0.5rem;
--padding-3xl: 1.375rem;
--padding-12xs: 0.062rem;
--padding-7xl: 1.625rem;
--padding-4xs: 0.562rem;
--padding-51xl: 4.375rem;
--padding-49xl: 4.25rem;
--padding-15xl: 2.125rem;
--padding-26xl: 2.812rem;
--padding-12xl: 1.937rem;
--padding-13xl: 2rem;
--padding-7xs: 0.375rem;
--padding-33xl: 3.25rem;
--padding-11xs-7: 0.106rem;
--padding-2178xl: 137.312rem;
--padding-909xl: 58rem;
--padding-584xl: 37.687rem;
--padding-8xl: 1.687rem;
--padding-mini: 0.937rem;
--padding-25xl: 2.75rem;
--padding-9xs: 0.25rem;
--padding-11xl: 1.875rem;
--padding-20xl: 2.437rem;
--padding-2xl: 1.312rem;
--padding-smi: 0.812rem;
--padding-2xs: 0.687rem;
--padding-base-5: 0.968rem;
--padding-47xl-5: 4.156rem;
--padding-394xl: 25.812rem;
--padding-249xl: 16.75rem;
--padding-39xl: 3.625rem;
--padding-19xl: 2.375rem;
--padding-38xl: 3.562rem;
--padding-18xl: 2.312rem;
--padding-18xl-5: 2.343rem;

/* Border radiuses */
--br-8xs: 5px;
--br-5xs: 8px;
--br-9xs: 4px;
--br-xl: 20px;
--br-6xl: 25px;
--br-12xs: 1px;
--br-2xl: 21px;

/* Effects */
--shadow-1: 0px 1px 1px rgba(100, 116, 139, 0.06), 0px 1px 2px rgba(100, 116, 139, 0.1);
}
`;
