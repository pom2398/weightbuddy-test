import styled from "styled-components";

const PortletTitle = styled.div`
  width: 20.375rem;
  position: relative;
  font-weight: 500;
  display: inline-block;
  max-width: 100%;
  @media screen and (max-width: 450px) {
    font-size: var(--medium-16-size);
  }
`;
const B = styled.b`
  flex: 1;
  position: relative;
  line-height: 137.5%;
  display: inline-block;
  max-width: 100%;
  @media screen and (max-width: 1050px) {
    font-size: var(--font-size-7xl);
    line-height: 2.188rem;
  }
  @media screen and (max-width: 450px) {
    font-size: var(--font-size-lgi);
    line-height: 1.625rem;
  }
`;
const ImageArray = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-11xs) 0rem var(--padding-11xs-7);
  box-sizing: border-box;
  max-width: 100%;
  font-size: var(--font-size-13xl);
  color: var(--text-primary);
`;
const CardRoot = styled.div`
  flex: 1;
  box-shadow: var(--shadow-1);
  border-radius: var(--br-8xs);
  background-color: var(--white-ffffff);
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-13xl) var(--padding-5xl) var(--padding-33xl);
  box-sizing: border-box;
  gap: var(--gap-2xl);
  min-width: 18.75rem;
  max-width: 100%;
  text-align: left;
  font-size: var(--font-size-xl);
  color: var(--gray-72787f);
  font-family: var(--medium-16);
`;

const Card = ({ portletTitle, prop }) => {
  return (
    <CardRoot>
      <PortletTitle>{portletTitle}</PortletTitle>
      <ImageArray>
        <B>{prop}</B>
      </ImageArray>
    </CardRoot>
  );
};

export default Card;
