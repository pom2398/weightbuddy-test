import styled from "styled-components";

const FrameChild = styled.div`
  width: 14.75rem;
  height: 12.313rem;
  position: relative;
  background-color: var(--gray-f1f1f1);
  display: none;
  z-index: 0;
`;
const Div = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 1.875rem;
  z-index: 1;
`;
const Div1 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 2.813rem;
  z-index: 1;
`;
const FrameItem = styled.div`
  width: 79.375rem;
  height: 0.063rem;
  position: absolute;
  margin: 0 !important;
  top: 4.438rem;
  right: -64.625rem;
  border-top: 1px solid var(--gray-e8ebed);
  box-sizing: border-box;
  z-index: 1;
`;
const FrameInner = styled.div`
  width: 79.375rem;
  height: 0.063rem;
  position: absolute;
  margin: 0 !important;
  right: -64.625rem;
  bottom: 3.875rem;
  border-top: 1px solid var(--gray-e8ebed);
  box-sizing: border-box;
  z-index: 1;
`;
const RectangleParentRoot = styled.div`
  position: absolute;
  top: 3.25rem;
  left: 0rem;
  background-color: var(--gray-f1f1f1);
  width: 14.75rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-9xl) var(--padding-xl) var(--padding-xl)
    var(--padding-5xl);
  box-sizing: border-box;
  gap: var(--gap-21xl);
  text-align: left;
  font-size: var(--medium-16-size);
  color: var(--gray-26282b);
  font-family: var(--medium-16);
`;

const FrameComponent = () => {
  return (
    <RectangleParentRoot>
      <FrameChild />
      <Div>본사</Div>
      <Div1>입점사</Div1>
      <Div>합계</Div>
      <FrameItem />
      <FrameInner />
    </RectangleParentRoot>
  );
};

export default FrameComponent;
