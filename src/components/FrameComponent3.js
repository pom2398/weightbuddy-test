import styled from "styled-components";
import Card from "./Card";

const Home = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 2.438rem;
`;
const H = styled.h2`
  margin: 0;
  position: relative;
  font-size: var(--font-size-11xl);
  font-weight: 700;
  font-family: inherit;
  color: var(--gray-26282b);
  display: inline-block;
  min-width: 6.938rem;
  @media screen and (max-width: 1050px) {
    font-size: var(--font-size-5xl);
  }
  @media screen and (max-width: 450px) {
    font-size: var(--font-size-lg);
  }
`;
const DataAggregator = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-xl) 0rem 0rem;
  gap: var(--gap-9xs);
`;
const CardParent = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-28xl);
  max-width: 100%;
  font-size: var(--font-size-xl);
  @media screen and (max-width: 750px) {
    gap: var(--gap-4xl);
  }
`;
const DataAggregatorParentRoot = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem 0rem var(--padding-7xs) var(--padding-xl);
  box-sizing: border-box;
  gap: var(--gap-34xl);
  max-width: 100%;
  text-align: left;
  font-size: var(--medium-14-size);
  color: var(--gray-72787f);
  font-family: var(--medium-16);
  @media screen and (max-width: 750px) {
    gap: var(--gap-7xl);
  }
`;

const FrameComponent3 = () => {
  return (
    <DataAggregatorParentRoot>
      <DataAggregator>
        <Home>home</Home>
        <H>대시보드</H>
      </DataAggregator>
      <CardParent>
        <Card portletTitle="등록상품 수" prop="40건" />
        <Card portletTitle="입점상품 수" prop="320건" />
        <Card portletTitle="정산 신청" prop="300,502,000원" />
      </CardParent>
    </DataAggregatorParentRoot>
  );
};

export default FrameComponent3;
