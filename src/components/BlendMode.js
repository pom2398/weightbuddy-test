import styled from "styled-components";
import Box1 from "./Box1";
import FrameComponent1 from "./FrameComponent1";
import Box from "./Box";
import FrameComponent from "./FrameComponent";

const Title = styled.b`
  height: 2.5rem;
  width: 13.625rem;
  position: absolute;
  margin: 0 !important;
  top: 0.75rem;
  left: 1.875rem;
  display: flex;
  align-items: center;
  @media screen and (max-width: 450px) {
    font-size: var(--medium-16-size);
  }
`;
const CornerRadius = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  position: relative;
  max-width: 100%;
`;
const InputFilter = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  max-width: 100%;
`;
const HeaderChild = styled.div`
  align-self: stretch;
  height: 3.75rem;
  position: relative;
  background-color: var(--gray-f1f1f1);
  display: none;
  z-index: 0;
`;
const KeyboardArrowDownIcon = styled.img`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  display: none;
`;
const Category = styled.div`
  width: 1.5rem;
  height: 1.5rem;
  background-color: var(--gray-f1f1f1);
  display: none;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  z-index: 1;
`;
const Label = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 1.875rem;
`;
const Category1 = styled.div`
  background-color: var(--gray-f1f1f1);
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-6xs);
  z-index: 1;
`;
const Label1 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 3.688rem;
`;
const CategoryWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-sm-5) 0rem 0rem;
`;
const Label2 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 3.688rem;
  z-index: 2;
`;
const CategoryParent = styled.div`
  width: 67.281rem;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: space-between;
  gap: var(--gap-xl);
  max-width: 100%;
`;
const HeaderInner = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: center;
  padding: 0rem var(--padding-xl) 0rem var(--padding-3xl);
  box-sizing: border-box;
  max-width: 100%;
`;
const HeaderItem = styled.div`
  align-self: stretch;
  height: 0.063rem;
  position: relative;
  border-top: 1px solid var(--gray-c9cdce);
  box-sizing: border-box;
  flex-shrink: 0;
  z-index: 2;
`;
const LineDiv = styled.div`
  width: calc(100% + 1px);
  height: 0.063rem;
  position: absolute;
  margin: 0 !important;
  top: 0.063rem;
  right: -0.062rem;
  left: 0rem;
  border-top: 1px solid var(--gray-c9cdce);
  box-sizing: border-box;
  z-index: 2;
`;
const Header = styled.div`
  position: absolute;
  top: 0rem;
  left: 0rem;
  background-color: var(--gray-f1f1f1);
  width: 100%;
  height: 3.75rem;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-start;
  padding: var(--padding-lg) 0rem var(--padding-lgi);
  box-sizing: border-box;
  gap: var(--gap-lgi);
  max-width: 100%;
  z-index: 1;
  text-align: left;
  color: var(--gray-26282b);
  @media screen and (max-width: 750px) {
    height: auto;
  }
`;
const Div = styled.div`
  position: absolute;
  top: 5rem;
  left: 21.125rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div1 = styled.div`
  position: absolute;
  top: 5rem;
  left: 37.688rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div2 = styled.div`
  position: absolute;
  top: 5rem;
  left: 54.25rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div3 = styled.div`
  position: absolute;
  top: 5rem;
  left: 70.5rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div4 = styled.div`
  position: absolute;
  top: 8.938rem;
  left: 21.125rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div5 = styled.div`
  position: absolute;
  top: 8.938rem;
  left: 37.688rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div6 = styled.div`
  position: absolute;
  top: 8.938rem;
  left: 54.25rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div7 = styled.div`
  position: absolute;
  top: 8.938rem;
  left: 70.5rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div8 = styled.div`
  position: absolute;
  top: 12.875rem;
  left: 21.125rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div9 = styled.div`
  position: absolute;
  top: 12.875rem;
  left: 37.688rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div10 = styled.div`
  position: absolute;
  top: 12.875rem;
  left: 54.25rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div11 = styled.div`
  position: absolute;
  top: 12.875rem;
  left: 70.5rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const FrameChild = styled.div`
  position: absolute;
  top: 15.563rem;
  left: 0rem;
  border-top: 1px solid var(--gray-c9cdce);
  box-sizing: border-box;
  width: 79.375rem;
  height: 0.063rem;
  z-index: 1;
`;
const HeaderParent = styled.div`
  height: 15.625rem;
  flex: 1;
  position: relative;
  max-width: 100%;
  @media screen and (max-width: 750px) {
    height: auto;
    min-height: 250;
  }
`;
const InputFilterInner = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-xs);
  box-sizing: border-box;
  max-width: 100%;
`;
const InputFilter1 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  max-width: 100%;
  text-align: center;
  font-size: var(--medium-16-size);
  color: var(--gray-72787f);
`;
const Category2 = styled.div`
  background-color: var(--gray-f1f1f1);
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-6xs);
  z-index: 1;
  text-align: left;
`;
const CategoryWrapper1 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-4xl-5) 0rem 0rem;
`;
const Label3 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 1.875rem;
  z-index: 2;
`;
const CategoryGroup = styled.div`
  width: 66.094rem;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: space-between;
  gap: var(--gap-xl);
  max-width: 100%;
`;
const FrameDiv = styled.div`
  width: 78.281rem;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: center;
  padding: 0rem var(--padding-xl);
  box-sizing: border-box;
  max-width: 100%;
`;
const Header1 = styled.div`
  position: absolute;
  top: 0rem;
  left: 0rem;
  background-color: var(--gray-f1f1f1);
  width: 100%;
  height: 3.75rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-lg) 0rem var(--padding-lgi);
  box-sizing: border-box;
  gap: var(--gap-lgi);
  max-width: 100%;
  z-index: 1;
  color: var(--gray-26282b);
  @media screen and (max-width: 450px) {
    height: auto;
  }
`;
const HeaderGroup = styled.div`
  height: 15.625rem;
  flex: 1;
  position: relative;
  max-width: 100%;
  @media screen and (max-width: 450px) {
    height: auto;
    min-height: 250;
  }
`;
const LargeComplexTable = styled.div`
  align-self: stretch;
  flex: 1;
  border-radius: var(--br-8xs);
  background-color: var(--white-ffffff);
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-4xl);
  max-width: 100%;
`;
const DataAggregator = styled.div`
  align-self: stretch;
  flex: 1;
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem 0rem 0rem var(--padding-xl);
  box-sizing: border-box;
  max-width: 100%;
`;
const BlendModeRoot = styled.div`
  align-self: stretch;
  height: 93.875rem;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-end;
  padding: 0rem var(--padding-11xs) 0rem 0rem;
  box-sizing: border-box;
  max-width: 100%;
  text-align: left;
  font-size: var(--font-size-xl);
  color: var(--gray-26282b);
  font-family: var(--medium-16);
`;

const BlendMode = () => {
  return (
    <BlendModeRoot>
      <DataAggregator>
        <LargeComplexTable>
          <InputFilter>
            <CornerRadius>
              <Title>주문 / 결제 진행 상황</Title>
              <Box1 propAlignSelf="unset" propFlex="1" propPadding="unset" />
            </CornerRadius>
            <FrameComponent1 />
          </InputFilter>
          <InputFilter>
            <Box title="상품 현황" />
            <FrameComponent1 />
          </InputFilter>
          <InputFilter1>
            <Box title="교환/반품 진행 현황" />
            <InputFilterInner>
              <HeaderParent>
                <Header>
                  <HeaderChild />
                  <Category>
                    <KeyboardArrowDownIcon
                      alt=""
                      src="/keyboard-arrow-down.svg"
                    />
                  </Category>
                  <HeaderInner>
                    <CategoryParent>
                      <Category1>
                        <Label>구분</Label>
                        <KeyboardArrowDownIcon
                          alt=""
                          src="/keyboard-arrow-down1.svg"
                        />
                      </Category1>
                      <CategoryWrapper>
                        <Category1>
                          <Label1>교환접수</Label1>
                          <KeyboardArrowDownIcon
                            alt=""
                            src="/keyboard-arrow-down1.svg"
                          />
                        </Category1>
                      </CategoryWrapper>
                      <CategoryWrapper>
                        <Category1>
                          <Label1>교환진행</Label1>
                          <KeyboardArrowDownIcon
                            alt=""
                            src="/keyboard-arrow-down1.svg"
                          />
                        </Category1>
                      </CategoryWrapper>
                      <CategoryWrapper>
                        <Category1>
                          <Label1>반품접수</Label1>
                          <KeyboardArrowDownIcon
                            alt=""
                            src="/keyboard-arrow-down1.svg"
                          />
                        </Category1>
                      </CategoryWrapper>
                      <Label2>반품진행</Label2>
                    </CategoryParent>
                  </HeaderInner>
                  <HeaderItem />
                  <LineDiv />
                </Header>
                <Div>0건</Div>
                <Div1>0건</Div1>
                <Div2>0건</Div2>
                <Div3>0건</Div3>
                <Div4>0건</Div4>
                <Div5>0건</Div5>
                <Div6>0건</Div6>
                <Div7>0건</Div7>
                <FrameComponent />
                <Div8>0건</Div8>
                <Div9>0건</Div9>
                <Div10>0건</Div10>
                <Div11>0건</Div11>
                <FrameChild />
              </HeaderParent>
            </InputFilterInner>
          </InputFilter1>
          <InputFilter1>
            <Box title="상품 승인 현황" />
            <InputFilterInner>
              <HeaderGroup>
                <Header1>
                  <HeaderChild />
                  <FrameDiv>
                    <CategoryGroup>
                      <Category2>
                        <Label>구분</Label>
                        <KeyboardArrowDownIcon
                          alt=""
                          src="/keyboard-arrow-down1.svg"
                        />
                      </Category2>
                      <CategoryWrapper>
                        <Category1>
                          <Label1>임시저장</Label1>
                          <KeyboardArrowDownIcon
                            alt=""
                            src="/keyboard-arrow-down1.svg"
                          />
                        </Category1>
                      </CategoryWrapper>
                      <CategoryWrapper>
                        <Category1>
                          <Label1>승인요청</Label1>
                          <KeyboardArrowDownIcon
                            alt=""
                            src="/keyboard-arrow-down1.svg"
                          />
                        </Category1>
                      </CategoryWrapper>
                      <CategoryWrapper1>
                        <Category1>
                          <Label1>승인완료</Label1>
                          <KeyboardArrowDownIcon
                            alt=""
                            src="/keyboard-arrow-down1.svg"
                          />
                        </Category1>
                      </CategoryWrapper1>
                      <Label3>반려</Label3>
                    </CategoryGroup>
                  </FrameDiv>
                  <HeaderItem />
                  <LineDiv />
                </Header1>
                <Div>0건</Div>
                <Div1>0건</Div1>
                <Div2>0건</Div2>
                <Div3>0건</Div3>
                <Div4>0건</Div4>
                <Div5>0건</Div5>
                <Div6>0건</Div6>
                <Div7>0건</Div7>
                <FrameComponent />
                <Div8>0건</Div8>
                <Div9>0건</Div9>
                <Div10>0건</Div10>
                <Div11>0건</Div11>
                <FrameChild />
              </HeaderGroup>
            </InputFilterInner>
          </InputFilter1>
        </LargeComplexTable>
      </DataAggregator>
    </BlendModeRoot>
  );
};

export default BlendMode;
