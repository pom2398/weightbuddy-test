import styled from "styled-components";

const HeaderChild = styled.div`
  align-self: stretch;
  height: 3.75rem;
  position: relative;
  background-color: var(--gray-f1f1f1);
  display: none;
  z-index: 0;
`;
const KeyboardArrowDownIcon = styled.img`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  display: none;
`;
const Category = styled.div`
  width: 1.5rem;
  height: 1.5rem;
  background-color: var(--gray-f1f1f1);
  display: none;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  z-index: 1;
`;
const Label = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 1.875rem;
`;
const Category1 = styled.div`
  background-color: var(--gray-f1f1f1);
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-6xs);
  z-index: 1;
`;
const CategoryWrapper = styled.div`
  width: 5.156rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
`;
const Label1 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 3.688rem;
`;
const Label2 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 3.688rem;
  z-index: 2;
`;
const Label3 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 2.813rem;
  z-index: 2;
`;
const FrameParent = styled.div`
  width: 66.469rem;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  gap: var(--gap-xl);
  max-width: 100%;
  @media screen and (max-width: 750px) {
    flex-wrap: wrap;
  }
`;
const HeaderInner = styled.div`
  width: 78.656rem;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: center;
  padding: 0rem var(--padding-xl);
  box-sizing: border-box;
  max-width: 100%;
`;
const HeaderItem = styled.div`
  align-self: stretch;
  height: 0.063rem;
  position: relative;
  border-top: 1px solid var(--gray-c9cdce);
  box-sizing: border-box;
  flex-shrink: 0;
  z-index: 2;
`;
const LineDiv = styled.div`
  width: calc(100% + 1px);
  height: 0.063rem;
  position: absolute;
  margin: 0 !important;
  top: 0.063rem;
  right: -0.062rem;
  left: 0rem;
  border-top: 1px solid var(--gray-c9cdce);
  box-sizing: border-box;
  z-index: 2;
`;
const Header = styled.div`
  position: absolute;
  top: 0rem;
  left: 0rem;
  background-color: var(--gray-f1f1f1);
  width: 100%;
  height: 3.75rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-lg) 0rem var(--padding-lgi);
  box-sizing: border-box;
  gap: var(--gap-lgi);
  max-width: 100%;
  z-index: 1;
  text-align: left;
  color: var(--gray-26282b);
  @media screen and (max-width: 750px) {
    height: auto;
  }
`;
const Div = styled.div`
  position: absolute;
  top: 5rem;
  left: 21.125rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div1 = styled.div`
  position: absolute;
  top: 5rem;
  left: 33.563rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div2 = styled.div`
  position: absolute;
  top: 5rem;
  left: 46rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div3 = styled.div`
  position: absolute;
  top: 5rem;
  left: 58.438rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div4 = styled.div`
  position: absolute;
  top: 5rem;
  left: 70.5rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div5 = styled.div`
  position: absolute;
  top: 8.938rem;
  left: 21.125rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div6 = styled.div`
  position: absolute;
  top: 8.938rem;
  left: 33.563rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div7 = styled.div`
  position: absolute;
  top: 8.938rem;
  left: 46rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div8 = styled.div`
  position: absolute;
  top: 8.938rem;
  left: 58.438rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div9 = styled.div`
  position: absolute;
  top: 8.938rem;
  left: 70.5rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const FrameChild = styled.div`
  width: 14.75rem;
  height: 12.313rem;
  position: relative;
  background-color: var(--gray-f1f1f1);
  display: none;
  z-index: 0;
`;
const Div10 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 1.875rem;
  z-index: 1;
`;
const Div11 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 2.813rem;
  z-index: 1;
`;
const FrameItem = styled.div`
  width: 79.375rem;
  height: 0.063rem;
  position: absolute;
  margin: 0 !important;
  top: 4.438rem;
  right: -64.625rem;
  border-top: 1px solid var(--gray-e8ebed);
  box-sizing: border-box;
  z-index: 1;
`;
const FrameInner = styled.div`
  width: 79.375rem;
  height: 0.063rem;
  position: absolute;
  margin: 0 !important;
  right: -64.625rem;
  bottom: 3.875rem;
  border-top: 1px solid var(--gray-e8ebed);
  box-sizing: border-box;
  z-index: 1;
`;
const RectangleParent = styled.div`
  position: absolute;
  top: 3.25rem;
  left: 0rem;
  background-color: var(--gray-f1f1f1);
  width: 14.75rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-9xl) var(--padding-xl) var(--padding-xl)
    var(--padding-5xl);
  box-sizing: border-box;
  gap: var(--gap-21xl);
  text-align: left;
  color: var(--gray-26282b);
`;
const Div12 = styled.div`
  position: absolute;
  top: 12.875rem;
  left: 21.125rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div13 = styled.div`
  position: absolute;
  top: 12.875rem;
  left: 33.563rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div14 = styled.div`
  position: absolute;
  top: 12.875rem;
  left: 46rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div15 = styled.div`
  position: absolute;
  top: 12.875rem;
  left: 58.438rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const Div16 = styled.div`
  position: absolute;
  top: 12.875rem;
  left: 70.5rem;
  font-weight: 500;
  display: inline-block;
  min-width: 1.5rem;
`;
const FrameChild1 = styled.div`
  position: absolute;
  top: 15.563rem;
  left: 0rem;
  border-top: 1px solid var(--gray-c9cdce);
  box-sizing: border-box;
  width: 79.375rem;
  height: 0.063rem;
  z-index: 1;
`;
const HeaderParent = styled.div`
  height: 15.625rem;
  flex: 1;
  position: relative;
  max-width: 100%;
  @media screen and (max-width: 750px) {
    height: auto;
    min-height: 250;
  }
`;
const InputFilterInnerRoot = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-xs);
  box-sizing: border-box;
  max-width: 100%;
  text-align: center;
  font-size: var(--medium-16-size);
  color: var(--gray-72787f);
  font-family: var(--medium-16);
`;

const FrameComponent1 = () => {
  return (
    <InputFilterInnerRoot>
      <HeaderParent>
        <Header>
          <HeaderChild />
          <Category>
            <KeyboardArrowDownIcon alt="" src="/keyboard-arrow-down.svg" />
          </Category>
          <HeaderInner>
            <FrameParent>
              <CategoryWrapper>
                <Category1>
                  <Label>구분</Label>
                  <KeyboardArrowDownIcon
                    alt=""
                    src="/keyboard-arrow-down1.svg"
                  />
                </Category1>
              </CategoryWrapper>
              <Category1>
                <Label1>주문접수</Label1>
                <KeyboardArrowDownIcon alt="" src="/keyboard-arrow-down1.svg" />
              </Category1>
              <Category1>
                <Label1>결제완료</Label1>
                <KeyboardArrowDownIcon alt="" src="/keyboard-arrow-down1.svg" />
              </Category1>
              <Category1>
                <Label1>주문확인</Label1>
                <KeyboardArrowDownIcon alt="" src="/keyboard-arrow-down1.svg" />
              </Category1>
              <Label2>상품준비</Label2>
              <Label3>배송중</Label3>
            </FrameParent>
          </HeaderInner>
          <HeaderItem />
          <LineDiv />
        </Header>
        <Div>0건</Div>
        <Div1>0건</Div1>
        <Div2>0건</Div2>
        <Div3>0건</Div3>
        <Div4>0건</Div4>
        <Div5>0건</Div5>
        <Div6>0건</Div6>
        <Div7>0건</Div7>
        <Div8>0건</Div8>
        <Div9>0건</Div9>
        <RectangleParent>
          <FrameChild />
          <Div10>본사</Div10>
          <Div11>입점사</Div11>
          <Div10>합계</Div10>
          <FrameItem />
          <FrameInner />
        </RectangleParent>
        <Div12>0건</Div12>
        <Div13>0건</Div13>
        <Div14>0건</Div14>
        <Div15>0건</Div15>
        <Div16>0건</Div16>
        <FrameChild1 />
      </HeaderParent>
    </InputFilterInnerRoot>
  );
};

export default FrameComponent1;
