import styled from "styled-components";
import Box1 from "./Box1";

const Button = styled.b`
  position: relative;
  line-height: 1.5rem;
  display: inline-block;
  min-width: 5.438rem;
`;
const ButtonText = styled.div`
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;
const ButtonIcon = styled.img`
  height: 1.25rem;
  width: 1rem;
  position: relative;
  transform: rotate(-180deg);
`;
const Content = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  gap: var(--gap-5xs);
`;
const UnstyledButton = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  padding: var(--padding-5xs) var(--padding-base);
`;
const Button1 = styled.div`
  border-radius: var(--br-5xs);
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  transform: rotate(180deg);
`;
const Overline = styled.b`
  align-self: stretch;
  position: relative;
  line-height: 1.75rem;
  @media screen and (max-width: 450px) {
    font-size: var(--medium-16-size);
    line-height: 1.375rem;
  }
`;
const Typography = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  transform: rotate(180deg);
  min-width: 44.75rem;
  max-width: 100%;
  font-size: var(--font-size-xl);
  color: var(--gray-26282b);
  @media screen and (max-width: 1050px) {
    min-width: 100%;
  }
`;
const Cardheader = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
  justify-content: flex-start;
  padding: var(--padding-13xl) var(--padding-5xl) var(--padding-5xl);
  box-sizing: border-box;
  transform: rotate(180deg);
  max-width: 100%;
  row-gap: 20px;
`;
const K = styled.div`
  align-self: stretch;
  position: relative;
  line-height: 1rem;
  mix-blend-mode: normal;
`;
const K1 = styled.div`
  width: 1.438rem;
  position: relative;
  line-height: 1rem;
  display: inline-block;
  mix-blend-mode: normal;
  min-width: 1.438rem;
`;
const K2 = styled.div`
  width: 1.063rem;
  position: relative;
  line-height: 1rem;
  display: inline-block;
  mix-blend-mode: normal;
  min-width: 1.063rem;
`;
const Div = styled.div`
  width: 0.563rem;
  position: relative;
  line-height: 1rem;
  display: inline-block;
  mix-blend-mode: normal;
  min-width: 0.563rem;
`;
const Yvaluelabels = styled.div`
  width: 2.438rem;
  overflow: hidden;
  flex-shrink: 0;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-start;
  gap: var(--gap-20xl-6);
  opacity: 0.85;
  mix-blend-mode: normal;
`;
const FrameChild = styled.img`
  width: 100%;
  height: 100%;
  position: absolute;
  margin: 0 !important;
  top: 0rem;
  right: 0rem;
  bottom: 0rem;
  left: 0rem;
  max-width: 100%;
  overflow: hidden;
  max-height: 100%;
`;
const BarIcon = styled.img`
  height: 11.813rem;
  width: 0.875rem;
  position: relative;
  z-index: 1;
`;
const Bar = styled.div`
  height: 7.813rem;
  width: 0.875rem;
  position: relative;
  border-radius: var(--br-9xs) var(--br-9xs) 0px 0px;
  background-color: var(--gray-e8ebed);
  z-index: 1;
`;
const BarParent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-start;
  gap: var(--gap-6xs);
`;
const BarIcon1 = styled.img`
  height: 3.375rem;
  width: 0.875rem;
  position: relative;
  z-index: 1;
`;
const Bar1 = styled.div`
  align-self: stretch;
  width: 0.875rem;
  position: relative;
  border-radius: var(--br-9xs) var(--br-9xs) 0px 0px;
  background-color: var(--gray-e8ebed);
  z-index: 1;
`;
const BarGroup = styled.div`
  height: 13.875rem;
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-start;
  gap: var(--gap-6xs);
`;
const BarIcon2 = styled.img`
  height: 13.063rem;
  width: 0.875rem;
  position: relative;
  z-index: 1;
`;
const Bar2 = styled.div`
  height: 8.5rem;
  width: 0.875rem;
  position: relative;
  border-radius: var(--br-9xs) var(--br-9xs) 0px 0px;
  background-color: var(--gray-e8ebed);
  z-index: 1;
`;
const BarIcon3 = styled.img`
  height: 18.75rem;
  width: 0.875rem;
  position: relative;
  z-index: 1;
`;
const FrameDiv = styled.div`
  height: 20.375rem;
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-start;
  gap: var(--gap-6xs);
`;
const BarIcon4 = styled.img`
  height: 20rem;
  width: 0.875rem;
  position: relative;
  z-index: 1;
`;
const BarParent1 = styled.div`
  height: 17.188rem;
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-start;
  gap: var(--gap-6xs);
`;
const Bar3 = styled.div`
  height: 9.188rem;
  width: 0.875rem;
  position: relative;
  border-radius: var(--br-9xs) var(--br-9xs) 0px 0px;
  background-color: var(--gray-e8ebed);
  z-index: 1;
`;
const StackLayout = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: space-between;
  max-width: 100%;
  gap: var(--gap-xl);
  @media screen and (max-width: 750px) {
    flex-wrap: wrap;
    justify-content: center;
  }
`;
const FlowLayout = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-end;
  padding: 0rem var(--padding-3xl) 0rem var(--padding-12xl);
  box-sizing: border-box;
  max-width: 100%;
`;
const Aug = styled.div`
  margin-top: -0.25rem;
  width: 5.05rem;
  position: relative;
  font-weight: 500;
  display: inline-block;
  flex-shrink: 0;
`;
const Aug1 = styled.div`
  margin-top: -0.25rem;
  width: 5.113rem;
  position: relative;
  font-weight: 500;
  display: inline-block;
  flex-shrink: 0;
`;
const XaxisTicksLabels = styled.div`
  align-self: stretch;
  height: 1.125rem;
  overflow: hidden;
  flex-shrink: 0;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  padding: 0rem var(--padding-12xs) 0rem 0rem;
  box-sizing: border-box;
  gap: var(--gap-xl);
  z-index: 1;
`;
const FrameParent = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-start;
  padding: var(--padding-51xl) var(--padding-49xl) var(--padding-xl);
  box-sizing: border-box;
  position: relative;
  gap: var(--gap-xs);
  max-width: 100%;
  @media screen and (max-width: 1200px) {
    padding-left: var(--padding-15xl);
    padding-right: var(--padding-15xl);
    box-sizing: border-box;
  }
  @media screen and (max-width: 450px) {
    padding-top: var(--padding-26xl);
    box-sizing: border-box;
  }
`;
const FrameWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 0rem 0rem;
  box-sizing: border-box;
  min-width: 48.125rem;
  max-width: 100%;
  text-align: center;
  font-size: var(--medium-16-size);
  color: var(--gray-72787f);
  @media screen and (max-width: 1050px) {
    min-width: 100%;
  }
`;
const YvaluelabelsParent = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-6xl);
  max-width: 100%;
`;
const CheckboxFieldInner = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-7xl) 0rem var(--padding-lgi);
  box-sizing: border-box;
  max-width: 100%;
`;
const CheckboxField = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-sm);
  max-width: 100%;
  text-align: right;
  font-size: var(--font-size-xs);
  color: var(--color-gray-100);
`;
const Card = styled.div`
  flex: 1;
  box-shadow: var(--shadow-1);
  border-radius: var(--br-8xs);
  background-color: var(--white-ffffff);
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem 0rem var(--padding-5xs);
  box-sizing: border-box;
  gap: var(--gap-45xl);
  max-width: 100%;
  @media screen and (max-width: 750px) {
    gap: var(--gap-13xl);
  }
  @media screen and (max-width: 450px) {
    gap: var(--gap-base);
  }
`;
const CardWrapperRoot = styled.div`
  width: 80.938rem;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-end;
  padding: 0rem var(--padding-12xs);
  box-sizing: border-box;
  max-width: 100%;
  text-align: left;
  font-size: var(--medium-16-size);
  color: var(--main-0abeba);
  font-family: var(--medium-16);
`;

const FrameComponent2 = () => {
  return (
    <CardWrapperRoot>
      <Card>
        <Cardheader>
          <Button1>
            <UnstyledButton>
              <Content>
                <ButtonText>
                  <Button>Last 7 days</Button>
                </ButtonText>
                <ButtonIcon alt="" src="/buttonicon.svg" />
              </Content>
            </UnstyledButton>
          </Button1>
          <Typography>
            <Overline>최근 매출 추이</Overline>
          </Typography>
        </Cardheader>
        <CheckboxField>
          <CheckboxFieldInner>
            <YvaluelabelsParent>
              <Yvaluelabels>
                <K>20k</K>
                <K>20k</K>
                <K>20k</K>
                <K>20k</K>
                <K1>15k</K1>
                <K1>10k</K1>
                <K2>5k</K2>
                <Div>0</Div>
              </Yvaluelabels>
              <FrameWrapper>
                <FrameParent>
                  <FrameChild alt="" src="/group-3.svg" />
                  <FlowLayout>
                    <StackLayout>
                      <BarParent>
                        <BarIcon loading="lazy" alt="" src="/bar.svg" />
                        <Bar />
                      </BarParent>
                      <BarGroup>
                        <BarIcon1 loading="lazy" alt="" src="/bar-1.svg" />
                        <Bar1 />
                      </BarGroup>
                      <BarParent>
                        <BarIcon2 loading="lazy" alt="" src="/bar-2.svg" />
                        <Bar2 />
                      </BarParent>
                      <FrameDiv>
                        <BarIcon3 loading="lazy" alt="" src="/bar-3.svg" />
                        <Bar1 />
                      </FrameDiv>
                      <FrameDiv>
                        <BarIcon4 loading="lazy" alt="" src="/bar-4.svg" />
                        <Bar1 />
                      </FrameDiv>
                      <BarParent1>
                        <BarIcon2 loading="lazy" alt="" src="/bar-2.svg" />
                        <Bar1 />
                      </BarParent1>
                      <BarParent>
                        <BarIcon2 loading="lazy" alt="" src="/bar-2.svg" />
                        <Bar3 />
                      </BarParent>
                    </StackLayout>
                  </FlowLayout>
                  <XaxisTicksLabels>
                    <Aug>1 Aug</Aug>
                    <Aug>2 Aug</Aug>
                    <Aug>3 Aug</Aug>
                    <Aug1>4 Aug</Aug1>
                    <Aug>5 Aug</Aug>
                    <Aug>6 Aug</Aug>
                    <Aug>6 Aug</Aug>
                  </XaxisTicksLabels>
                </FrameParent>
              </FrameWrapper>
            </YvaluelabelsParent>
          </CheckboxFieldInner>
          <Box1 />
        </CheckboxField>
      </Card>
    </CardWrapperRoot>
  );
};

export default FrameComponent2;
