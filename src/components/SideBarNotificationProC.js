import { useCallback } from "react";
import styled from "styled-components";
import { useNavigate } from "react-router-dom";

const Icon = styled.img`
  height: 2.563rem;
  width: 3.188rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  object-fit: contain;
`;
const VectorIcon = styled.img`
  align-self: stretch;
  height: 1.875rem;
  position: relative;
  max-width: 100%;
  overflow: hidden;
  flex-shrink: 0;
`;
const VectorWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-end;
  padding: 0rem 0rem var(--padding-9xs);
`;
const Parent1 = styled.div`
  width: 12.094rem;
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-start;
  gap: var(--gap-lgi);
`;
const SidebarNotificationProCInner = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-25xl) var(--padding-4xs);
  @media screen and (max-width: 450px) {
    padding-left: var(--padding-xl);
    padding-right: var(--padding-xl);
    box-sizing: border-box;
  }
`;
const SeparatorIcon = styled.img`
  align-self: stretch;
  height: 0.063rem;
  position: relative;
  max-width: 100%;
  overflow: hidden;
  flex-shrink: 0;
`;
const GroupIcon = styled.img`
  position: absolute;
  height: 100%;
  width: 100%;
  top: 0%;
  right: 0%;
  bottom: 0%;
  left: 0%;
  max-width: 100%;
  overflow: hidden;
  max-height: 100%;
`;
const AdminPanelSettingsChild = styled.div`
  position: absolute;
  top: 0.313rem;
  left: 0.313rem;
  border-radius: var(--br-12xs);
  background-color: var(--gray-72787f);
  width: 0.438rem;
  height: 0.438rem;
  z-index: 1;
`;
const AdminPanelSettingsItem = styled.div`
  position: absolute;
  top: 0.813rem;
  left: 0.313rem;
  border-radius: var(--br-12xs);
  background-color: var(--gray-72787f);
  width: 0.438rem;
  height: 0.438rem;
  z-index: 1;
`;
const AdminPanelSettingsInner = styled.div`
  position: absolute;
  top: 0.313rem;
  left: 0.813rem;
  border-radius: var(--br-12xs);
  background-color: var(--gray-72787f);
  width: 0.438rem;
  height: 0.438rem;
  z-index: 1;
`;
const RectangleDiv = styled.div`
  position: absolute;
  top: 0.813rem;
  left: 0.813rem;
  border-radius: var(--br-12xs);
  background-color: var(--gray-72787f);
  width: 0.438rem;
  height: 0.438rem;
  z-index: 1;
`;
const AdminPanelSettings = styled.div`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
`;
const Pagetitle = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 3.688rem;
`;
const PagetitleWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-12xs) 0rem 0rem;
`;
const AdminPanelSettingsParent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-xl) 0rem 0rem;
  gap: var(--gap-xs);
`;
const AdminPanelSettings1 = styled.img`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  min-height: 1.5rem;
`;
const Pagetitle1 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 4.875rem;
`;
const AdminPanelSettingsGroup = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-xs);
`;
const Inner = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-7xs) 0rem 0rem;
`;
const Active = styled.div`
  height: 2.25rem;
  width: 0.25rem;
  position: relative;
  border-radius: var(--br-6xl);
`;
const Active1 = styled.div`
  position: absolute;
  top: 1.375rem;
  left: 0.25rem;
  border-radius: var(--br-6xl);
  background-color: var(--main-0abeba);
  width: 0.25rem;
  height: 2.25rem;
`;
const Active2 = styled.div`
  position: absolute;
  top: 0rem;
  left: 0rem;
  border-radius: var(--br-6xl);
  width: 0.25rem;
  height: 2.25rem;
`;
const ActiveParent = styled.div`
  height: 3.625rem;
  width: 0.5rem;
  position: absolute;
  margin: 0 !important;
  top: -2.75rem;
  right: 0.125rem;
`;
const Div = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  padding: 0rem var(--padding-7xs) 0rem 0rem;
  position: relative;
  gap: var(--gap-xl);
  cursor: pointer;
`;
const Pagetitle2 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 3.938rem;
`;
const Div1 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  padding: 0rem var(--padding-7xs) 0rem 0rem;
  gap: var(--gap-xl);
  cursor: pointer;
`;
const Pagetitle3 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 5.75rem;
`;
const FrameGroup = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  padding: 0rem var(--padding-7xs) 0rem 0rem;
  gap: var(--gap-xl);
`;
const BoxParent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-mini) 0rem 0rem;
  gap: var(--gap-xs);
`;
const FrameParent = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-8xl);
`;
const FrameWrapper = styled.div`
  width: 7.125rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-7xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Active3 = styled.div`
  width: 0.25rem;
  height: 2.25rem;
  position: relative;
  border-radius: var(--br-6xl);
`;
const ActiveGroup = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-mini);
`;
const Active4 = styled.div`
  position: absolute;
  top: 0rem;
  left: 0rem;
  border-radius: var(--br-6xl);
  width: 100%;
  height: 100%;
`;
const Authentication = styled.div`
  width: 0.25rem;
  height: 2.25rem;
  position: relative;
`;
const AuthenticationWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-end;
  padding: 0rem 0rem var(--padding-base);
`;
const FrameParent1 = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-start;
  gap: var(--gap-11xs);
`;
const FrameParent2 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  gap: var(--gap-xl);
`;
const GroupIcon1 = styled.img`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  min-height: 1.5rem;
`;
const FrameContainer = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-2xs);
`;
const FrameParent3 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem 0rem 0rem var(--padding-8xl);
  gap: var(--gap-mini-8);
`;
const SidebarNotificationProCRoot = styled.div`
  align-self: stretch;
  background-color: var(--white-ffffff);
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-start;
  padding: var(--padding-33xl) 0rem var(--padding-2178xl);
  gap: var(--gap-9xl);
  flex-shrink: 0;
  debug_commit: 1de1738;
  text-align: left;
  font-size: var(--medium-16-size);
  color: var(--gray-72787f);
  font-family: var(--medium-16);
  @media screen and (max-width: 1200px) {
    padding-top: var(--padding-3xl);
    padding-bottom: var(--padding-909xl);
    box-sizing: border-box;
  }
  @media screen and (max-width: 750px) {
    padding-top: var(--padding-xl);
    padding-bottom: var(--padding-584xl);
    box-sizing: border-box;
  }
`;

const SideBarNotificationProC = () => {
  const navigate = useNavigate();

  const onContainerClick = useCallback(() => {
    navigate("/");
  }, [navigate]);

  const onContainer1Click = useCallback(() => {
    navigate("/1");
  }, [navigate]);

  return (
    <SidebarNotificationProCRoot>
      <SidebarNotificationProCInner>
        <Parent1>
          <Icon loading="lazy" alt="" src="/--1.svg" />
          <VectorWrapper>
            <VectorIcon loading="lazy" alt="" src="/vector.svg" />
          </VectorWrapper>
        </Parent1>
      </SidebarNotificationProCInner>
      <SeparatorIcon alt="" src="/separator.svg" />
      <FrameParent3>
        <AdminPanelSettingsParent>
          <AdminPanelSettings>
            <GroupIcon loading="lazy" alt="" />
            <AdminPanelSettingsChild />
            <AdminPanelSettingsItem />
            <AdminPanelSettingsInner />
            <RectangleDiv />
          </AdminPanelSettings>
          <PagetitleWrapper>
            <Pagetitle>대시보드</Pagetitle>
          </PagetitleWrapper>
        </AdminPanelSettingsParent>
        <Div onClick={onContainerClick}>
          <Inner>
            <AdminPanelSettingsGroup>
              <AdminPanelSettings1
                loading="lazy"
                alt=""
                src="/admin-panel-settings.svg"
              />
              <PagetitleWrapper>
                <Pagetitle1>관리자 관리</Pagetitle1>
              </PagetitleWrapper>
            </AdminPanelSettingsGroup>
          </Inner>
          <Active />
          <ActiveParent>
            <Active1 />
            <Active2 />
          </ActiveParent>
        </Div>
        <Div1 onClick={onContainer1Click}>
          <Inner>
            <AdminPanelSettingsGroup>
              <AdminPanelSettings1
                loading="lazy"
                alt=""
                src="/admin-panel-settings-1.svg"
              />
              <PagetitleWrapper>
                <Pagetitle2>칼럼 관리</Pagetitle2>
              </PagetitleWrapper>
            </AdminPanelSettingsGroup>
          </Inner>
          <Active />
        </Div1>
        <FrameGroup>
          <Inner>
            <AdminPanelSettingsGroup>
              <AdminPanelSettings1
                loading="lazy"
                alt=""
                src="/admin-panel-settings-2.svg"
              />
              <PagetitleWrapper>
                <Pagetitle3>카테고리 관리</Pagetitle3>
              </PagetitleWrapper>
            </AdminPanelSettingsGroup>
          </Inner>
          <Active />
        </FrameGroup>
        <FrameContainer>
          <FrameParent2>
            <FrameWrapper>
              <FrameParent>
                <BoxParent>
                  <AdminPanelSettings1 loading="lazy" alt="" src="/box.svg" />
                  <PagetitleWrapper>
                    <Pagetitle2>상품 관리</Pagetitle2>
                  </PagetitleWrapper>
                </BoxParent>
                <AdminPanelSettingsGroup>
                  <AdminPanelSettings1
                    loading="lazy"
                    alt=""
                    src="/admin-panel-settings-3.svg"
                  />
                  <PagetitleWrapper>
                    <Pagetitle1>입점사 관리</Pagetitle1>
                  </PagetitleWrapper>
                </AdminPanelSettingsGroup>
              </FrameParent>
            </FrameWrapper>
            <FrameParent1>
              <ActiveGroup>
                <Active3 />
                <Active3 />
              </ActiveGroup>
              <AuthenticationWrapper>
                <Authentication>
                  <Active4 />
                </Authentication>
              </AuthenticationWrapper>
            </FrameParent1>
          </FrameParent2>
          <FrameGroup>
            <Inner>
              <AdminPanelSettingsGroup>
                <GroupIcon1 alt="" src="/group-1.svg" />
                <PagetitleWrapper>
                  <Pagetitle2>회원 목록</Pagetitle2>
                </PagetitleWrapper>
              </AdminPanelSettingsGroup>
            </Inner>
            <Active />
          </FrameGroup>
        </FrameContainer>
      </FrameParent3>
    </SidebarNotificationProCRoot>
  );
};

export default SideBarNotificationProC;
