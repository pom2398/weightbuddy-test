import styled from "styled-components";

const Button = styled.b`
  position: relative;
  font-size: var(--medium-16-size);
  line-height: 1.5rem;
  display: inline-block;
  font-family: var(--medium-16);
  color: var(--main-0abeba);
  text-align: left;
  min-width: 3.813rem;
`;
const ButtonText = styled.div`
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;
const ButtonIcon = styled.img`
  height: 1.25rem;
  width: 1rem;
  position: relative;
`;
const Content = styled.button`
  cursor: pointer;
  border: none;
  padding: 0;
  background-color: transparent;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  gap: var(--gap-5xs);
`;
const UnstyledButton = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  padding: var(--padding-5xs) var(--padding-base);
`;
const Button1 = styled.div`
  border-radius: var(--br-5xs);
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
const Title = styled.b`
  width: 13.625rem;
  height: 2.5rem;
  position: absolute;
  margin: 0 !important;
  top: 0.75rem;
  left: 1.875rem;
  display: flex;
  align-items: center;
  z-index: 3;
  @media screen and (max-width: 450px) {
    font-size: var(--medium-16-size);
  }
`;
const BoxRoot = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-start;
  padding: var(--padding-base);
  position: relative;
  z-index: 2;
  text-align: left;
  font-size: var(--font-size-xl);
  color: var(--gray-26282b);
  font-family: var(--medium-16);
`;

const Box = ({ title }) => {
  return (
    <BoxRoot>
      <Button1>
        <UnstyledButton>
          <Content>
            <ButtonText>
              <Button>View all</Button>
            </ButtonText>
            <ButtonIcon alt="" src="/buttonicon-1.svg" />
          </Content>
        </UnstyledButton>
      </Button1>
      <Title>{title}</Title>
    </BoxRoot>
  );
};

export default Box;
