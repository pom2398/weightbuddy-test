import { useState } from "react";
import styled from "styled-components";

const Button = styled.div`
  position: relative;
  font-size: var(--font-size-mini);
  line-height: 1.625rem;
  font-weight: 500;
  font-family: var(--medium-16);
  color: var(--gray-26282b);
  text-align: left;
  display: inline-block;
  min-width: 1.75rem;
`;
const ButtonText = styled.div`
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;
const UnstyledButton = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  padding: var(--padding-2xs) var(--padding-5xl);
`;
const Button1 = styled.button`
  cursor: pointer;
  border: none;
  padding: 0;
  background-color: var(--gray-f1f1f1);
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
const Box = styled.div`
  width: 100%;
  margin: 0 !important;
  position: absolute;
  top: 0rem;
  right: 0rem;
  left: 0rem;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-start;
  padding: var(--padding-base);
  box-sizing: border-box;
  max-width: 100%;
`;
const B = styled.b``;
const Span = styled.span`
  font-weight: 500;
`;
const TitleTxt = styled.span``;
const Title = styled.div`
  height: 2.5rem;
  flex: 1;
  position: relative;
  display: flex;
  align-items: center;
  z-index: 1;
  @media screen and (max-width: 450px) {
    font-size: var(--medium-16-size);
  }
`;
const MatrixSolver = styled.div`
  width: 15.875rem;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-lg);
  box-sizing: border-box;
`;
const HeaderChild = styled.div`
  align-self: stretch;
  height: 3.75rem;
  position: relative;
  background-color: var(--gray-f1f1f1);
  display: none;
`;
const HeaderItem = styled.div`
  align-self: stretch;
  height: 0.063rem;
  position: relative;
  border-top: 1px solid var(--gray-c9cdce);
  box-sizing: border-box;
  z-index: 2;
`;
const FilledtogglecheckBoxOutlin = styled.input`
  margin: 0;
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
`;
const Padding = styled.div`
  height: 2.625rem;
  width: 2.625rem;
  border-radius: var(--br-2xl);
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs);
  box-sizing: border-box;
`;
const Checkbox = styled.div`
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  z-index: 1;
`;
const Label = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 1.375rem;
  z-index: 1;
`;
const KeyboardArrowDownIcon = styled.img`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  min-height: 1.5rem;
  z-index: 1;
`;
const LabelParent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-6xs);
`;
const FrameWrapper = styled.div`
  width: 6.188rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Label1 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 2.813rem;
  z-index: 1;
`;
const FrameContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) var(--padding-18xl-5) 0rem 0rem;
`;
const Label2 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 1.875rem;
  z-index: 1;
`;
const FrameDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 0rem 0rem;
`;
const CheckboxParent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-7xl-5);
  max-width: 100%;
  @media screen and (max-width: 450px) {
    flex-wrap: wrap;
  }
`;
const Label3 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 3.688rem;
  z-index: 1;
`;
const Label4 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 5.75rem;
  z-index: 1;
`;
const FrameGroup = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  gap: var(--gap-xl);
`;
const FrameWrapper1 = styled.div`
  width: 18.313rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 0rem 0rem;
  box-sizing: border-box;
`;
const FrameParent = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  max-width: 100%;
  gap: var(--gap-xl);
  @media screen and (max-width: 850px) {
    flex-wrap: wrap;
  }
`;
const HeaderInner = styled.div`
  width: 77.438rem;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-smi);
  box-sizing: border-box;
  max-width: 100%;
`;
const Header = styled.div`
  align-self: stretch;
  background-color: var(--gray-f1f1f1);
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-5xs);
  max-width: 100%;
  z-index: 1;
  color: var(--gray-26282b);
`;
const Checkbox1 = styled.div`
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
`;
const Div = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 1.75rem;
`;
const CheckboxGroup = styled.div`
  width: 6.563rem;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-16xl);
`;
const Div1 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 1.188rem;
`;
const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) var(--padding-3xs) 0rem 0rem;
`;
const Div2 = styled.div`
  position: relative;
  font-weight: 500;
`;
const Frame = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-3xs) 0rem 0rem;
  text-align: left;
`;
const FrameParent1 = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-85xl);
  max-width: 100%;
  @media screen and (max-width: 850px) {
    flex-wrap: wrap;
    gap: var(--gap-33xl);
  }
  @media screen and (max-width: 450px) {
    gap: var(--gap-7xl);
  }
`;
const Div3 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 5.313rem;
`;
const Div4 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 0.625rem;
`;
const FrameWrapper2 = styled.div`
  width: 14.313rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 0rem 0rem;
  box-sizing: border-box;
`;
const FrameParent2 = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  max-width: 100%;
  gap: var(--gap-xl);
  @media screen and (max-width: 1225px) {
    flex-wrap: wrap;
  }
`;
const GraphAnalyzerPlusInner = styled.div`
  width: 73.625rem;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-smi);
  box-sizing: border-box;
  max-width: 100%;
`;
const GraphAnalyzerPlusChild = styled.div`
  align-self: stretch;
  height: 0.063rem;
  position: relative;
  border-top: 1px solid var(--gray-e8ebed);
  box-sizing: border-box;
`;
const FilledtogglecheckBox = styled.input`
  accent-color: #0abeba;
  margin: 0;
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
`;
const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-3xs) 0rem 0rem;
`;
const Wrapper1 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-3xs) var(--padding-2xs) 0rem 0rem;
`;
const Wrapper2 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-2xs) 0rem 0rem;
  text-align: left;
`;
const FrameParent3 = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-80xl);
  max-width: 100%;
  @media screen and (max-width: 850px) {
    flex-wrap: wrap;
    gap: var(--gap-30xl);
  }
`;
const FrameWrapper3 = styled.div`
  width: 14.313rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-end;
  padding: 0rem 0rem var(--padding-4xs);
  box-sizing: border-box;
`;
const FrameParent4 = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: space-between;
  max-width: 100%;
  gap: var(--gap-xl);
  @media screen and (max-width: 850px) {
    flex-wrap: wrap;
  }
`;
const FrameParent5 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-2xs);
  max-width: 100%;
`;
const Wrapper3 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-2xs) 0rem 0rem;
`;
const Div5 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 2.625rem;
`;
const Wrapper4 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-2xs) var(--padding-2xs) 0rem 0rem;
`;
const Wrapper5 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-xs) 0rem 0rem;
  text-align: left;
`;
const FrameParent6 = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-73xl);
  max-width: 100%;
  @media screen and (max-width: 850px) {
    flex-wrap: wrap;
    gap: var(--gap-27xl);
  }
  @media screen and (max-width: 450px) {
    gap: var(--gap-4xl);
  }
`;
const FrameWrapper4 = styled.div`
  width: 14.313rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-end;
  padding: 0rem 0rem var(--padding-5xs);
  box-sizing: border-box;
`;
const FrameParent7 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-5xs);
  max-width: 100%;
`;
const FrameParent8 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-smi);
  max-width: 100%;
`;
const MergeStrategyPlus = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-80xl);
  max-width: 100%;
  @media screen and (max-width: 850px) {
    flex-wrap: wrap;
    gap: var(--gap-30xl);
  }
  @media screen and (max-width: 450px) {
    gap: var(--gap-6xl);
  }
`;
const TreeTraverserPlus = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-xl);
  max-width: 100%;
`;
const FrameParent9 = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-89xl);
  max-width: 100%;
  @media screen and (max-width: 850px) {
    flex-wrap: wrap;
    gap: var(--gap-35xl);
  }
  @media screen and (max-width: 450px) {
    gap: var(--gap-8xl);
  }
`;
const TreeTraverserPlusItem = styled.div`
  align-self: stretch;
  height: 0.063rem;
  position: relative;
  border-top: 1px solid var(--gray-c9cdce);
  box-sizing: border-box;
`;
const GraphAnalyzerPlus = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-4xs);
  max-width: 100%;
  text-align: center;
  font-size: var(--medium-16-size);
  color: var(--gray-72787f);
`;
const ParallelHandler = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-2xl);
  max-width: 100%;
`;
const KeyboardArrowDown = styled.img`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  object-fit: contain;
  min-height: 1.5rem;
`;
const KeyboardArrowDownParent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-lgi);
`;
const LargeComplexTableInner = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: center;
  padding: 0rem var(--padding-xl) 0rem var(--padding-2xl);
  text-align: center;
  font-size: var(--medium-16-size);
`;
const LargeComplexTable = styled.div`
  flex: 1;
  border-radius: var(--br-8xs);
  background-color: var(--white-ffffff);
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-start;
  padding: var(--padding-xs) var(--padding-xs) var(--padding-38xl);
  box-sizing: border-box;
  position: relative;
  gap: var(--gap-20xl);
  max-width: 100%;
  @media screen and (max-width: 850px) {
    gap: var(--gap-lgi);
    padding-top: var(--padding-xl);
    padding-bottom: var(--padding-18xl);
    box-sizing: border-box;
  }
`;
const PathPlannerRoot = styled.div`
  align-self: stretch;
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem 0rem var(--padding-39xl) var(--padding-xl);
  box-sizing: border-box;
  max-width: 100%;
  text-align: left;
  font-size: var(--font-size-xl);
  color: var(--gray-26282b);
  font-family: var(--medium-16);
  @media screen and (max-width: 850px) {
    padding-bottom: var(--padding-19xl);
    box-sizing: border-box;
  }
`;

const PathPlanner = () => {
  const [filledtogglecheckBoxIconChecked, setFilledtogglecheckBoxIconChecked] =
    useState(true);
  return (
    <PathPlannerRoot>
      <LargeComplexTable>
        <Box>
          <Button1>
            <UnstyledButton>
              <ButtonText>
                <Button>삭제</Button>
              </ButtonText>
            </UnstyledButton>
          </Button1>
        </Box>
        <ParallelHandler>
          <MatrixSolver>
            <Title>
              <TitleTxt>
                <B>5건</B>
                <Span>의 검색 결과</Span>
              </TitleTxt>
            </Title>
          </MatrixSolver>
          <GraphAnalyzerPlus>
            <Header>
              <HeaderChild />
              <HeaderItem />
              <HeaderInner>
                <FrameParent>
                  <CheckboxParent>
                    <Checkbox>
                      <Padding>
                        <FilledtogglecheckBoxOutlin type="checkbox" />
                      </Padding>
                    </Checkbox>
                    <FrameWrapper>
                      <LabelParent>
                        <Label>No</Label>
                        <KeyboardArrowDownIcon
                          alt=""
                          src="/keyboard-arrow-down1.svg"
                        />
                      </LabelParent>
                    </FrameWrapper>
                    <FrameContainer>
                      <LabelParent>
                        <Label1>조회수</Label1>
                        <KeyboardArrowDownIcon
                          alt=""
                          src="/keyboard-arrow-down1.svg"
                        />
                      </LabelParent>
                    </FrameContainer>
                    <FrameDiv>
                      <LabelParent>
                        <Label2>제목</Label2>
                        <KeyboardArrowDownIcon
                          alt=""
                          src="/keyboard-arrow-down1.svg"
                        />
                      </LabelParent>
                    </FrameDiv>
                  </CheckboxParent>
                  <FrameWrapper1>
                    <FrameGroup>
                      <LabelParent>
                        <Label3>작성일자</Label3>
                        <KeyboardArrowDownIcon
                          alt=""
                          src="/keyboard-arrow-down1.svg"
                        />
                      </LabelParent>
                      <LabelParent>
                        <Label4>카테고리 이름</Label4>
                        <KeyboardArrowDownIcon
                          alt=""
                          src="/keyboard-arrow-down1.svg"
                        />
                      </LabelParent>
                    </FrameGroup>
                  </FrameWrapper1>
                </FrameParent>
              </HeaderInner>
              <HeaderItem />
            </Header>
            <GraphAnalyzerPlusInner>
              <FrameParent2>
                <FrameParent1>
                  <CheckboxGroup>
                    <Checkbox1>
                      <Padding>
                        <FilledtogglecheckBoxOutlin type="checkbox" />
                      </Padding>
                    </Checkbox1>
                    <FrameDiv>
                      <Div>001</Div>
                    </FrameDiv>
                  </CheckboxGroup>
                  <Container>
                    <Div1>22</Div1>
                  </Container>
                  <Frame>
                    <Div2>왜 1월 1일이면 헬스장에 사람이 많아질까</Div2>
                  </Frame>
                </FrameParent1>
                <FrameWrapper2>
                  <FrameGroup>
                    <Div3>2024-01-31</Div3>
                    <Div4>1</Div4>
                  </FrameGroup>
                </FrameWrapper2>
              </FrameParent2>
            </GraphAnalyzerPlusInner>
            <GraphAnalyzerPlusChild />
            <FrameParent8>
              <FrameParent7>
                <FrameParent5>
                  <GraphAnalyzerPlusInner>
                    <FrameParent4>
                      <FrameParent3>
                        <CheckboxGroup>
                          <Checkbox1>
                            <Padding>
                              <FilledtogglecheckBox
                                checked={filledtogglecheckBoxIconChecked}
                                type="checkbox"
                                onChange={(event) =>
                                  setFilledtogglecheckBoxIconChecked(
                                    event.target.checked
                                  )
                                }
                              />
                            </Padding>
                          </Checkbox1>
                          <Wrapper>
                            <Div>002</Div>
                          </Wrapper>
                        </CheckboxGroup>
                        <Wrapper1>
                          <Div>141</Div>
                        </Wrapper1>
                        <Wrapper2>
                          <Div2>토마토 레시피 11가지</Div2>
                        </Wrapper2>
                      </FrameParent3>
                      <FrameWrapper3>
                        <FrameGroup>
                          <Div3>2024-01-24</Div3>
                          <Div4>2</Div4>
                        </FrameGroup>
                      </FrameWrapper3>
                    </FrameParent4>
                  </GraphAnalyzerPlusInner>
                  <GraphAnalyzerPlusChild />
                </FrameParent5>
                <GraphAnalyzerPlusInner>
                  <FrameParent4>
                    <FrameParent6>
                      <CheckboxGroup>
                        <Checkbox1>
                          <Padding>
                            <FilledtogglecheckBoxOutlin type="checkbox" />
                          </Padding>
                        </Checkbox1>
                        <Wrapper3>
                          <Div>003</Div>
                        </Wrapper3>
                      </CheckboxGroup>
                      <Wrapper4>
                        <Div5>1,525</Div5>
                      </Wrapper4>
                      <Wrapper5>
                        <Div2>닭가슴살로 맛있게 요리하기</Div2>
                      </Wrapper5>
                    </FrameParent6>
                    <FrameWrapper4>
                      <FrameGroup>
                        <Div3>2024-01-02</Div3>
                        <Div4>3</Div4>
                      </FrameGroup>
                    </FrameWrapper4>
                  </FrameParent4>
                </GraphAnalyzerPlusInner>
              </FrameParent7>
              <GraphAnalyzerPlusChild />
            </FrameParent8>
            <TreeTraverserPlus>
              <GraphAnalyzerPlusInner>
                <FrameParent4>
                  <MergeStrategyPlus>
                    <CheckboxGroup>
                      <Checkbox1>
                        <Padding>
                          <FilledtogglecheckBoxOutlin type="checkbox" />
                        </Padding>
                      </Checkbox1>
                      <Wrapper3>
                        <Div>003</Div>
                      </Wrapper3>
                    </CheckboxGroup>
                    <Wrapper4>
                      <Div>214</Div>
                    </Wrapper4>
                    <Wrapper5>
                      <Div2>닭가슴살로 맛있게 요리하기</Div2>
                    </Wrapper5>
                  </MergeStrategyPlus>
                  <FrameWrapper4>
                    <FrameGroup>
                      <Div3>2024-01-02</Div3>
                      <Div4>3</Div4>
                    </FrameGroup>
                  </FrameWrapper4>
                </FrameParent4>
              </GraphAnalyzerPlusInner>
              <GraphAnalyzerPlusChild />
            </TreeTraverserPlus>
            <FrameParent8>
              <GraphAnalyzerPlusInner>
                <FrameParent4>
                  <FrameParent9>
                    <CheckboxGroup>
                      <Checkbox1>
                        <Padding>
                          <FilledtogglecheckBoxOutlin type="checkbox" />
                        </Padding>
                      </Checkbox1>
                      <Wrapper3>
                        <Div>003</Div>
                      </Wrapper3>
                    </CheckboxGroup>
                    <Wrapper4>
                      <Div4>4</Div4>
                    </Wrapper4>
                    <Wrapper5>
                      <Div2>닭가슴살로 맛있게 요리하기</Div2>
                    </Wrapper5>
                  </FrameParent9>
                  <FrameWrapper4>
                    <FrameGroup>
                      <Div3>2024-01-02</Div3>
                      <Div4>3</Div4>
                    </FrameGroup>
                  </FrameWrapper4>
                </FrameParent4>
              </GraphAnalyzerPlusInner>
              <TreeTraverserPlusItem />
            </FrameParent8>
          </GraphAnalyzerPlus>
        </ParallelHandler>
        <LargeComplexTableInner>
          <KeyboardArrowDownParent>
            <KeyboardArrowDown
              loading="lazy"
              alt=""
              src="/keyboard-arrow-down-9@2x.png"
            />
            <Div4>1</Div4>
            <Div4>2</Div4>
            <Div4>3</Div4>
            <Div4>4</Div4>
            <Div4>5</Div4>
            <KeyboardArrowDown
              loading="lazy"
              alt=""
              src="/keyboard-arrow-down-10@2x.png"
            />
          </KeyboardArrowDownParent>
        </LargeComplexTableInner>
      </LargeComplexTable>
    </PathPlannerRoot>
  );
};

export default PathPlanner;
