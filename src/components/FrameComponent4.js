import styled from "styled-components";

const Pages = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 6.438rem;
`;
const H = styled.h2`
  margin: 0;
  position: relative;
  font-size: inherit;
  font-weight: 700;
  font-family: inherit;
  display: inline-block;
  min-width: 7.375rem;
  @media screen and (max-width: 850px) {
    font-size: var(--font-size-5xl);
  }
  @media screen and (max-width: 450px) {
    font-size: var(--font-size-lg);
  }
`;
const Button = styled.div`
  position: relative;
  font-size: var(--medium-16-size);
  font-weight: 500;
  font-family: var(--medium-16);
  color: var(--white-ffffff);
  text-align: center;
  display: inline-block;
  min-width: 5.75rem;
`;
const Button1 = styled.button`
  cursor: pointer;
  border: none;
  padding: var(--padding-base-5) var(--padding-33xl);
  background-color: var(--gray-26282b);
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  box-sizing: border-box;
  min-width: 7.938rem;
  white-space: nowrap;
  &:hover {
    background-color: var(--color-dimgray);
  }
`;
const Button2 = styled.div`
  position: relative;
  font-size: var(--medium-16-size);
  font-weight: 500;
  font-family: var(--medium-16);
  color: var(--white-ffffff);
  text-align: center;
  display: inline-block;
  min-width: 3.938rem;
`;
const Button3 = styled.button`
  cursor: pointer;
  border: none;
  padding: var(--padding-base-5) var(--padding-47xl-5);
  background-color: var(--gray-26282b);
  flex: 0.6848;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  box-sizing: border-box;
  min-width: 7.938rem;
  white-space: nowrap;
  &:hover {
    background-color: var(--color-dimgray);
  }
  @media screen and (max-width: 450px) {
    flex: 1;
  }
`;
const GraphBuilder = styled.div`
  width: 25.625rem;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-lg);
  max-width: 100%;
  @media screen and (max-width: 450px) {
    flex-wrap: wrap;
  }
`;
const MatrixTransform = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  max-width: 100%;
  gap: var(--gap-xl);
  @media screen and (max-width: 850px) {
    flex-wrap: wrap;
  }
`;
const B = styled.b`
  position: relative;
  display: inline-block;
  min-width: 4.938rem;
  @media screen and (max-width: 450px) {
    font-size: var(--medium-16-size);
  }
`;
const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-lg);
`;
const Pagetitle = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 1.875rem;
`;
const KeyboardArrowDown = styled.img`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  min-height: 1.5rem;
`;
const SearchBar = styled.div`
  width: 13.25rem;
  background-color: var(--gray-f1f1f1);
  overflow: hidden;
  flex-shrink: 0;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  padding: var(--padding-mini) var(--padding-xl) var(--padding-mini)
    var(--padding-11xl);
  box-sizing: border-box;
  gap: var(--gap-xl);
`;
const Pagetitle1 = styled.div`
  position: relative;
  font-weight: 500;
`;
const SearchBar1 = styled.div`
  flex: 1;
  background-color: var(--gray-f1f1f1);
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-mini) var(--padding-xl);
  box-sizing: border-box;
  gap: var(--gap-2xl);
  min-width: 13.875rem;
  max-width: 100%;
  color: var(--gray-72787f);
`;
const Button4 = styled.div`
  position: relative;
  font-size: var(--medium-16-size);
  font-weight: 500;
  font-family: var(--medium-16);
  color: var(--white-ffffff);
  text-align: center;
  display: inline-block;
  min-width: 1.875rem;
`;
const Button5 = styled.button`
  cursor: pointer;
  border: none;
  padding: var(--padding-base-5) var(--padding-xl);
  background-color: var(--gray-26282b);
  width: 12.25rem;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: center;
  box-sizing: border-box;
  &:hover {
    background-color: var(--color-dimgray);
  }
`;
const MergeBranch = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-xs);
  max-width: 100%;
  font-size: var(--medium-16-size);
  @media screen and (max-width: 850px) {
    flex-wrap: wrap;
  }
`;
const SearchBar2 = styled.div`
  width: 60.875rem;
  border-radius: var(--br-8xs);
  background-color: var(--white-ffffff);
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-lg) var(--padding-xs) var(--padding-11xl);
  box-sizing: border-box;
  gap: var(--gap-xl);
  max-width: 100%;
  font-size: var(--font-size-xl);
`;
const ParallelProcessor = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-25xl);
  max-width: 100%;
  font-size: var(--font-size-11xl);
  color: var(--gray-26282b);
  @media screen and (max-width: 850px) {
    gap: var(--gap-3xl);
  }
`;
const ErrorHandler = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-9xs);
  max-width: 100%;
`;
const IterationLoopRoot = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-end;
  padding: 0rem var(--padding-2xs) 0rem var(--padding-xl);
  box-sizing: border-box;
  max-width: 100%;
  text-align: left;
  font-size: var(--medium-14-size);
  color: var(--gray-72787f);
  font-family: var(--medium-16);
`;

const FrameComponent4 = () => {
  return (
    <IterationLoopRoot>
      <ErrorHandler>
        <Pages>Pages / 회원관리</Pages>
        <ParallelProcessor>
          <MatrixTransform>
            <H>칼럼 관리</H>
            <GraphBuilder>
              <Button1>
                <Button>카테고리 관리</Button>
              </Button1>
              <Button3>
                <Button2>신규 작성</Button2>
              </Button3>
            </GraphBuilder>
          </MatrixTransform>
          <SearchBar2>
            <Wrapper>
              <B>칼럼 검색</B>
            </Wrapper>
            <MergeBranch>
              <SearchBar>
                <Pagetitle>전체</Pagetitle>
                <KeyboardArrowDown alt="" src="/keyboard-arrow-down2.svg" />
              </SearchBar>
              <SearchBar1>
                <KeyboardArrowDown alt="" src="/search.svg" />
                <Pagetitle1>이름 / 아이디를 입력하여 검색</Pagetitle1>
              </SearchBar1>
              <Button5>
                <Button4>검색</Button4>
              </Button5>
            </MergeBranch>
          </SearchBar2>
        </ParallelProcessor>
      </ErrorHandler>
    </IterationLoopRoot>
  );
};

export default FrameComponent4;
