import SideBarNotificationProC from "../components/SideBarNotificationProC";
import styled from "styled-components";
import FrameComponent3 from "../components/FrameComponent3";
import FrameComponent2 from "../components/FrameComponent2";
import BlendMode from "../components/BlendMode";

const TagFacesIcon = styled.img`
  width: 1.5rem;
  height: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  debug_commit: 1de1738;
`;
const Sidebar = styled.div`
  height: 168.313rem;
  width: 18.125rem;
  border-radius: 0px 0px var(--br-xl) 0px;
  overflow: hidden;
  flex-shrink: 0;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem 0rem var(--padding-xs);
  box-sizing: border-box;
  gap: var(--gap-3xs);
  @media screen and (max-width: 1050px) {
    display: none;
  }
`;
const FrameParent = styled.section`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-start;
  gap: var(--gap-lg);
  max-width: 100%;
`;
const Inner = styled.main`
  width: 82.125rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-27xl) 0rem 0rem;
  box-sizing: border-box;
  max-width: calc(100% - 300px);
  flex-shrink: 0;
  @media screen and (max-width: 1200px) {
    padding-top: var(--padding-xl);
    box-sizing: border-box;
  }
  @media screen and (max-width: 1050px) {
    max-width: 100%;
  }
`;
const DivRoot = styled.div`
  width: 100%;
  height: 167.563rem;
  position: relative;
  background-color: var(--gray-f7f8f9);
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-10xs) 0rem 5.062rem;
  box-sizing: border-box;
  gap: var(--gap-3xs);
  line-height: normal;
  letter-spacing: normal;
  @media screen and (max-width: 1050px) {
    height: auto;
    padding-left: var(--padding-3xs);
    padding-right: var(--padding-3xs);
    box-sizing: border-box;
  }
`;

const Frame = () => {
  return (
    <DivRoot>
      <Sidebar>
        <SideBarNotificationProC />
        <TagFacesIcon alt="" src="/tag-faces.svg" />
      </Sidebar>
      <Inner>
        <FrameParent>
          <FrameComponent3 />
          <FrameComponent2 />
          <BlendMode />
        </FrameParent>
      </Inner>
    </DivRoot>
  );
};

export default Frame;
