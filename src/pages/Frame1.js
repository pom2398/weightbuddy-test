import { useState } from "react";
import styled from "styled-components";

const Active = styled.div`
  position: absolute;
  top: 0rem;
  left: 0rem;
  border-radius: var(--br-6xl);
  width: 100%;
  height: 100%;
`;
const Authentication = styled.div`
  height: 2.25rem;
  width: 0.25rem;
  position: relative;
  display: none;
`;
const Pagetitle = styled.div`
  position: relative;
  font-weight: 500;
  display: none;
  min-width: 3.938rem;
`;
const Li = styled.li``;
const Ul = styled.ul`
  margin: 0;
  font-family: inherit;
  font-size: inherit;
  padding-left: var(--padding-2xl);
`;
const Pagetitle1 = styled.div`
  width: 5.438rem;
  position: relative;
  font-weight: 500;
  display: none;
`;
const Pagetitle2 = styled.div`
  position: relative;
  font-weight: 500;
  color: var(--gray-72787f);
  display: none;
`;
const Icon = styled.img`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  display: none;
`;
const Active1 = styled.div`
  height: 2.25rem;
  width: 0.25rem;
  position: relative;
  border-radius: var(--br-6xl);
  display: none;
`;
const Pagetitle3 = styled.div`
  position: relative;
  font-weight: 500;
  display: none;
  min-width: 6.688rem;
`;
const Pagetitle4 = styled.div`
  position: relative;
  font-weight: 500;
  display: none;
  min-width: 4.875rem;
`;
const Active2 = styled.div`
  height: 2.25rem;
  width: 0.25rem;
  position: relative;
  border-radius: var(--br-6xl);
  background-color: var(--main-0abeba);
  display: none;
`;
const SeparatorIcon = styled.img`
  height: 0.063rem;
  width: 18.125rem;
  position: relative;
  display: none;
`;
const Icon1 = styled.img`
  height: 2.563rem;
  width: 3.188rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  object-fit: contain;
  display: none;
`;
const VectorIcon = styled.img`
  height: 1.875rem;
  width: 7.719rem;
  position: relative;
  display: none;
`;
const Icon2 = styled.img`
  height: 2.563rem;
  width: 3.188rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  object-fit: contain;
`;
const VectorIcon1 = styled.img`
  align-self: stretch;
  height: 1.875rem;
  position: relative;
  max-width: 100%;
  overflow: hidden;
  flex-shrink: 0;
`;
const VectorWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-end;
  padding: 0rem 0rem var(--padding-9xs);
`;
const Parent1 = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-start;
  gap: var(--gap-lgi);
`;
const SidebarNotificationProCInner = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-33xl) 0rem var(--padding-25xl);
  @media screen and (max-width: 450px) {
    padding-left: var(--padding-xl);
    padding-right: var(--padding-xl);
    box-sizing: border-box;
  }
`;
const SeparatorIcon1 = styled.img`
  align-self: stretch;
  height: 0.063rem;
  position: relative;
  max-width: 100%;
  overflow: hidden;
  flex-shrink: 0;
`;
const GroupIcon = styled.img`
  position: absolute;
  height: 100%;
  width: 100%;
  top: 0%;
  right: 0%;
  bottom: 0%;
  left: 0%;
  max-width: 100%;
  overflow: hidden;
  max-height: 100%;
`;
const AdminPanelSettingsChild = styled.div`
  position: absolute;
  top: 0.313rem;
  left: 0.313rem;
  border-radius: var(--br-12xs);
  background-color: var(--gray-72787f);
  width: 0.438rem;
  height: 0.438rem;
  z-index: 1;
`;
const AdminPanelSettingsItem = styled.div`
  position: absolute;
  top: 0.813rem;
  left: 0.313rem;
  border-radius: var(--br-12xs);
  background-color: var(--gray-72787f);
  width: 0.438rem;
  height: 0.438rem;
  z-index: 1;
`;
const AdminPanelSettingsInner = styled.div`
  position: absolute;
  top: 0.313rem;
  left: 0.813rem;
  border-radius: var(--br-12xs);
  background-color: var(--gray-72787f);
  width: 0.438rem;
  height: 0.438rem;
  z-index: 1;
`;
const RectangleDiv = styled.div`
  position: absolute;
  top: 0.813rem;
  left: 0.813rem;
  border-radius: var(--br-12xs);
  background-color: var(--gray-72787f);
  width: 0.438rem;
  height: 0.438rem;
  z-index: 1;
`;
const AdminPanelSettings = styled.div`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
`;
const Pagetitle5 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 3.688rem;
`;
const PagetitleWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-12xs) 0rem 0rem;
`;
const AdminPanelSettingsParent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-lgi) 0rem 0rem;
  gap: var(--gap-xs);
`;
const AdminPanelSettings1 = styled.img`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  min-height: 1.5rem;
`;
const Pagetitle6 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 4.875rem;
`;
const AdminPanelSettingsGroup = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-xs);
`;
const FrameContainer = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-xl);
`;
const FrameWrapper = styled.div`
  width: 7.125rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-7xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Active3 = styled.div`
  width: 0.25rem;
  height: 2.25rem;
  position: relative;
  border-radius: var(--br-6xl);
`;
const ActiveParent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-5xs);
`;
const Active4 = styled.div`
  width: 0.25rem;
  height: 2.25rem;
  position: relative;
  border-radius: var(--br-6xl);
  background-color: var(--main-0abeba);
`;
const ActiveWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-3xl) 0rem 0rem;
`;
const FrameDiv = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
`;
const FrameGroup = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  padding: 0rem var(--padding-11xs) 0rem 0rem;
  gap: var(--gap-xl);
`;
const Pagetitle7 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 3.938rem;
`;
const FrameWrapper1 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-7xs) 0rem 0rem;
`;
const Active5 = styled.div`
  height: 2.25rem;
  width: 0.25rem;
  position: relative;
  border-radius: var(--br-6xl);
`;
const FrameParent = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  padding: 0rem var(--padding-7xs) 0rem 0rem;
  gap: var(--gap-xl);
`;
const Pagetitle8 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 5.75rem;
`;
const BoxParent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-mini) 0rem 0rem;
  gap: var(--gap-xs);
`;
const FrameParent1 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-8xl);
`;
const ActiveGroup = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-mini);
`;
const Authentication1 = styled.div`
  width: 0.25rem;
  height: 2.25rem;
  position: relative;
`;
const AuthenticationWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-end;
  padding: 0rem 0rem var(--padding-base);
`;
const FrameParent2 = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-start;
  gap: var(--gap-11xs);
`;
const FrameParent3 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  gap: var(--gap-xl);
`;
const GroupIcon1 = styled.img`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  min-height: 1.5rem;
`;
const FrameParent4 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-2xs);
`;
const FrameParent5 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem 0rem 0rem var(--padding-8xl);
  gap: var(--gap-mini);
`;
const SeparatorParent = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-start;
  gap: var(--gap-3xl);
`;
const SidebarNotificationProC = styled.div`
  flex: 1;
  background-color: var(--white-ffffff);
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-33xl) 0rem var(--padding-394xl);
  gap: var(--gap-18xl);
  z-index: 2;
  color: var(--gray-72787f);
  @media screen and (max-width: 750px) {
    padding-top: var(--padding-15xl);
    padding-bottom: var(--padding-249xl);
    box-sizing: border-box;
  }
  @media screen and (max-width: 450px) {
    gap: var(--gap-lg);
  }
`;
const SidebarNotificationProC1 = styled.div`
  align-self: stretch;
  background-color: var(--white-ffffff);
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem 0rem 111.5rem;
  flex-shrink: 0;
  debug_commit: 1de1738;
  @media screen and (max-width: 1225px) {
    padding-bottom: 72.5rem;
    box-sizing: border-box;
  }
  @media screen and (max-width: 1050px) {
    padding-bottom: 47.125rem;
    box-sizing: border-box;
  }
  @media screen and (max-width: 750px) {
    padding-bottom: 30.625rem;
    box-sizing: border-box;
  }
`;
const TagFacesIcon = styled.img`
  width: 1.5rem;
  height: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  debug_commit: 1de1738;
`;
const Sidebar = styled.div`
  height: 168.313rem;
  width: 18.125rem;
  border-radius: 0px 0px var(--br-xl) 0px;
  overflow: hidden;
  flex-shrink: 0;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem 0rem var(--padding-xs);
  box-sizing: border-box;
  gap: var(--gap-3xs);
  @media screen and (max-width: 1050px) {
    display: none;
  }
`;
const Pages = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 6.438rem;
`;
const H = styled.h2`
  margin: 0;
  position: relative;
  font-size: inherit;
  font-weight: 700;
  font-family: inherit;
  @media screen and (max-width: 750px) {
    font-size: var(--font-size-5xl);
  }
  @media screen and (max-width: 450px) {
    font-size: var(--font-size-lg);
  }
`;
const B = styled.b`
  position: relative;
  display: inline-block;
  min-width: 6.063rem;
  @media screen and (max-width: 450px) {
    font-size: var(--medium-16-size);
  }
`;
const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-lg);
`;
const Pagetitle9 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 1.875rem;
`;
const SearchBar = styled.div`
  width: 13.25rem;
  background-color: var(--gray-f1f1f1);
  overflow: hidden;
  flex-shrink: 0;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  padding: var(--padding-mini) var(--padding-xl) var(--padding-mini)
    var(--padding-11xl);
  box-sizing: border-box;
  gap: var(--gap-xl);
`;
const Pagetitle10 = styled.div`
  position: relative;
  font-weight: 500;
`;
const SearchBar1 = styled.div`
  flex: 1;
  background-color: var(--gray-f1f1f1);
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-mini) var(--padding-xl);
  box-sizing: border-box;
  gap: var(--gap-2xl);
  min-width: 13.875rem;
  max-width: 100%;
  color: var(--gray-72787f);
`;
const Button = styled.div`
  position: relative;
  font-size: var(--medium-16-size);
  font-weight: 500;
  font-family: var(--medium-16);
  color: var(--white-ffffff);
  text-align: center;
  display: inline-block;
  min-width: 1.875rem;
`;
const Button1 = styled.button`
  cursor: pointer;
  border: none;
  padding: var(--padding-base-5) var(--padding-xl);
  background-color: var(--gray-26282b);
  width: 12.25rem;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: center;
  box-sizing: border-box;
  &:hover {
    background-color: var(--color-dimgray);
  }
`;
const SearchBarParent = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-xs);
  max-width: 100%;
  font-size: var(--medium-16-size);
  @media screen and (max-width: 1050px) {
    flex-wrap: wrap;
  }
`;
const SearchBar2 = styled.div`
  align-self: stretch;
  border-radius: var(--br-8xs);
  background-color: var(--white-ffffff);
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-lg) var(--padding-xs) var(--padding-11xl);
  box-sizing: border-box;
  gap: var(--gap-xl);
  max-width: 100%;
  font-size: var(--font-size-xl);
`;
const Group = styled.div`
  width: 60.875rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: 3.437rem;
  max-width: 100%;
  @media screen and (max-width: 750px) {
    gap: var(--gap-8xl);
  }
`;
const Button2 = styled.div`
  position: relative;
  font-size: var(--medium-16-size);
  font-weight: 500;
  font-family: var(--medium-16);
  color: var(--white-ffffff);
  text-align: center;
  display: inline-block;
  min-width: 3.938rem;
`;
const Button3 = styled.button`
  cursor: pointer;
  border: none;
  padding: var(--padding-base-5) var(--padding-47xl-5);
  background-color: var(--gray-26282b);
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  white-space: nowrap;
  &:hover {
    background-color: var(--color-dimgray);
  }
`;
const FrameParent6 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  max-width: 100%;
  gap: var(--gap-xl);
  font-size: var(--font-size-11xl);
  color: var(--gray-26282b);
  @media screen and (max-width: 1225px) {
    flex-wrap: wrap;
  }
`;
const PagesParent = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-9xs);
  max-width: 100%;
`;
const FrameWrapper2 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-end;
  padding: 0rem var(--padding-2xs) 0rem var(--padding-xl);
  box-sizing: border-box;
  max-width: 100%;
`;
const Button4 = styled.div`
  position: relative;
  font-size: var(--font-size-mini);
  line-height: 1.625rem;
  font-weight: 500;
  font-family: var(--medium-16);
  color: var(--gray-26282b);
  text-align: left;
  display: inline-block;
  min-width: 1.75rem;
`;
const ButtonText = styled.div`
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;
const UnstyledButton = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  padding: var(--padding-2xs) var(--padding-5xl);
`;
const Button5 = styled.button`
  cursor: pointer;
  border: none;
  padding: 0;
  background-color: var(--gray-f1f1f1);
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
const Box = styled.div`
  width: 100%;
  margin: 0 !important;
  position: absolute;
  top: 0rem;
  right: 0rem;
  left: 0rem;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-start;
  padding: var(--padding-base);
  box-sizing: border-box;
  max-width: 100%;
`;
const B1 = styled.b``;
const Span = styled.span`
  font-weight: 500;
`;
const TitleTxt = styled.span``;
const Title = styled.div`
  height: 2.5rem;
  flex: 1;
  position: relative;
  display: flex;
  align-items: center;
  z-index: 1;
  @media screen and (max-width: 450px) {
    font-size: var(--medium-16-size);
  }
`;
const TitleWrapper = styled.div`
  width: 15.875rem;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-lg);
  box-sizing: border-box;
`;
const FrameChild = styled.div`
  align-self: stretch;
  height: 3.75rem;
  position: relative;
  background-color: var(--gray-f1f1f1);
  display: none;
`;
const FrameItem = styled.div`
  align-self: stretch;
  height: 0.063rem;
  position: relative;
  border-top: 1px solid var(--gray-c9cdce);
  box-sizing: border-box;
  z-index: 2;
`;
const FilledtogglecheckBoxOutlin = styled.input`
  margin: 0;
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
`;
const Padding = styled.div`
  height: 2.625rem;
  width: 2.625rem;
  border-radius: var(--br-2xl);
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs);
  box-sizing: border-box;
`;
const Checkbox = styled.div`
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  z-index: 2;
`;
const Label = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 1.375rem;
  z-index: 2;
`;
const KeyboardArrowDownIcon = styled.img`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  min-height: 1.5rem;
  z-index: 2;
`;
const LabelParent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-6xs);
`;
const FrameWrapper3 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 0rem 0rem;
`;
const CheckboxParent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-7xl-5);
`;
const Label1 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 1.875rem;
  z-index: 2;
`;
const FrameWrapper4 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 1.031rem 0rem 0rem;
`;
const Label2 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 1.063rem;
  z-index: 2;
`;
const FrameWrapper5 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 1.406rem 0rem 0rem;
`;
const FrameWrapper6 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 0.718rem 0rem 0rem;
`;
const Label3 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 3.063rem;
  z-index: 2;
`;
const FrameWrapper7 = styled.div`
  width: 8.469rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 0rem 0rem;
  box-sizing: border-box;
`;
const FrameWrapper8 = styled.div`
  width: 7.344rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Label4 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 5.75rem;
  z-index: 2;
`;
const FrameWrapper9 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 0.468rem 0rem 0rem;
`;
const Label5 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 4.875rem;
  z-index: 2;
`;
const FrameParent7 = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: space-between;
  max-width: 100%;
  gap: var(--gap-xl);
`;
const FrameWrapper10 = styled.div`
  width: 77.188rem;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-smi);
  box-sizing: border-box;
  max-width: 100%;
`;
const RectangleParent = styled.div`
  align-self: stretch;
  background-color: var(--gray-f1f1f1);
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-12xs) 0rem 0rem;
  box-sizing: border-box;
  gap: var(--gap-5xs);
  max-width: 100%;
  z-index: 1;
  color: var(--gray-26282b);
`;
const Checkbox1 = styled.div`
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
`;
const Div = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 1.75rem;
`;
const Container = styled.div`
  width: 4.438rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Div1 = styled.div`
  position: relative;
  text-decoration: underline;
  font-weight: 500;
  display: inline-block;
  min-width: 2.813rem;
`;
const Frame = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) var(--padding-15xl) 0rem 0rem;
  color: var(--main-0abeba);
`;
const Dskfjd = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 4.125rem;
`;
const Dskfjd22Wrapper = styled.div`
  width: 6.625rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Div2 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 2.813rem;
`;
const Wrapper1 = styled.div`
  width: 7.313rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Div3 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 0.75rem;
`;
const Wrapper2 = styled.div`
  width: 6.063rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Div4 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 6.313rem;
`;
const Wrapper3 = styled.div`
  width: 10.75rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Div5 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 5.313rem;
`;
const Wrapper4 = styled.div`
  width: 10.188rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-4xs) 0rem 0rem;
  box-sizing: border-box;
`;
const CheckboxGroup = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  max-width: 100%;
  gap: var(--gap-xl);
  @media screen and (max-width: 1225px) {
    flex-wrap: wrap;
  }
`;
const FrameWrapper11 = styled.div`
  width: 74.875rem;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-smi);
  box-sizing: border-box;
  max-width: 100%;
`;
const LineDiv = styled.div`
  align-self: stretch;
  height: 0.063rem;
  position: relative;
  border-top: 1px solid var(--gray-e8ebed);
  box-sizing: border-box;
`;
const FilledtogglecheckBox = styled.input`
  accent-color: #0abeba;
  margin: 0;
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
`;
const Wrapper5 = styled.div`
  width: 4.438rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-3xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Wrapper6 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-3xs) var(--padding-20xl) 0rem 0rem;
  color: var(--main-0abeba);
`;
const Ssls = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 3.5rem;
`;
const SslsWrapper = styled.div`
  width: 6.313rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-3xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Wrapper7 = styled.div`
  width: 5.5rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-3xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Div6 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 4.375rem;
`;
const Wrapper8 = styled.div`
  width: 7.875rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-3xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Wrapper9 = styled.div`
  width: 10.75rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-3xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Wrapper10 = styled.div`
  width: 10.188rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-3xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Wrapper11 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-3xs) 0rem 0rem;
`;
const Wrapper12 = styled.div`
  width: 4.813rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-2xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Div7 = styled.div`
  position: relative;
  text-decoration: underline;
  font-weight: 500;
  display: inline-block;
  min-width: 2.125rem;
`;
const Wrapper13 = styled.div`
  width: 4.688rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-2xs) 0rem 0rem;
  box-sizing: border-box;
  color: var(--main-0abeba);
`;
const Fhdsdv = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 3.875rem;
`;
const Fhdsdv7Wrapper = styled.div`
  width: 6.5rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-2xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Wrapper14 = styled.div`
  width: 7.313rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-2xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Wrapper15 = styled.div`
  width: 6.063rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-2xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Wrapper16 = styled.div`
  width: 10.75rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-2xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Wrapper17 = styled.div`
  width: 10.188rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-2xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Wrapper18 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-2xs) 0rem 0rem;
`;
const FrameChild1 = styled.div`
  align-self: stretch;
  height: 0.063rem;
  position: relative;
  border-top: 1px solid var(--gray-c9cdce);
  box-sizing: border-box;
`;
const FrameParent8 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-xs);
  max-width: 100%;
`;
const FrameParent9 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: 0.6rem;
  max-width: 100%;
  text-align: center;
  font-size: var(--medium-16-size);
  color: var(--gray-72787f);
`;
const FrameParent10 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-xl);
  max-width: 100%;
`;
const KeyboardArrowDown = styled.img`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  object-fit: contain;
  min-height: 1.5rem;
`;
const Div8 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 0.625rem;
`;
const KeyboardArrowDownParent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-lgi);
`;
const LargeComplexTableInner = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: center;
  padding: 0rem var(--padding-xl) 0rem var(--padding-2xl);
  text-align: center;
  font-size: var(--medium-16-size);
`;
const LargeComplexTable = styled.div`
  flex: 1;
  border-radius: var(--br-8xs);
  background-color: var(--white-ffffff);
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-start;
  padding: var(--padding-xs) var(--padding-xs) var(--padding-20xl);
  box-sizing: border-box;
  position: relative;
  gap: var(--gap-20xl);
  max-width: 100%;
  @media screen and (max-width: 750px) {
    gap: var(--gap-lgi);
  }
  @media screen and (max-width: 450px) {
    padding-top: var(--padding-xl);
    padding-bottom: 1.563rem;
    box-sizing: border-box;
  }
`;
const LargeComplexTableWrapper = styled.div`
  align-self: stretch;
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem 0rem 0rem var(--padding-xl);
  box-sizing: border-box;
  max-width: 100%;
  font-size: var(--font-size-xl);
  color: var(--gray-26282b);
`;
const FrameParent11 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-start;
  gap: var(--gap-35xl);
  max-width: 100%;
  @media screen and (max-width: 750px) {
    gap: var(--gap-8xl);
  }
`;
const Inner = styled.section`
  width: 82rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-27xl) 0rem 0rem;
  box-sizing: border-box;
  max-width: calc(100% - 300px);
  text-align: left;
  font-size: var(--medium-14-size);
  color: var(--gray-72787f);
  font-family: var(--medium-16);
  @media screen and (max-width: 1050px) {
    padding-top: var(--padding-11xl);
    box-sizing: border-box;
    max-width: 100%;
  }
  @media screen and (max-width: 450px) {
    padding-top: var(--padding-xl);
    box-sizing: border-box;
  }
`;
const DivRoot = styled.div`
  width: 100%;
  height: 56.063rem;
  position: relative;
  background-color: var(--gray-f7f8f9);
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-10xs) 0rem 6.062rem;
  box-sizing: border-box;
  gap: var(--gap-3xs);
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  font-size: var(--medium-16-size);
  color: var(--gray-26282b);
  font-family: var(--medium-16);
  @media screen and (max-width: 1225px) {
    height: auto;
  }
  @media screen and (max-width: 1050px) {
    padding-left: var(--padding-3xs);
    padding-right: var(--padding-3xs);
    box-sizing: border-box;
  }
`;

const Frame1 = () => {
  const [filledtogglecheckBoxIconChecked, setFilledtogglecheckBoxIconChecked] =
    useState(true);
  return (
    <DivRoot>
      <Sidebar>
        <SidebarNotificationProC1>
          <Authentication>
            <Active />
          </Authentication>
          <Pagetitle>회원 관리</Pagetitle>
          <Pagetitle1>
            <Ul>
              <Li>회원 관리</Li>
            </Ul>
          </Pagetitle1>
          <Pagetitle2>
            <Ul>
              <Li>라이선스 키 발급</Li>
            </Ul>
          </Pagetitle2>
          <Icon alt="" src="/icon.svg" />
          <Active1 />
          <Pagetitle>기기 관리</Pagetitle>
          <Active1 />
          <Pagetitle3>수면컨디션 관리</Pagetitle3>
          <Active1 />
          <Pagetitle4>관리자 관리</Pagetitle4>
          <Icon alt="" src="/admin-panel-settings1.svg" />
          <Active1 />
          <Active2 />
          <SeparatorIcon alt="" src="/separator.svg" />
          <Icon alt="" src="/settings.svg" />
          <Icon alt="" src="/tag-faces1.svg" />
          <Icon1 alt="" src="/--11.svg" />
          <VectorIcon alt="" src="/vector.svg" />
          <SidebarNotificationProC>
            <SidebarNotificationProCInner>
              <Parent1>
                <Icon2 loading="lazy" alt="" src="/--1.svg" />
                <VectorWrapper>
                  <VectorIcon1 loading="lazy" alt="" src="/vector.svg" />
                </VectorWrapper>
              </Parent1>
            </SidebarNotificationProCInner>
            <SeparatorParent>
              <SeparatorIcon1 loading="lazy" alt="" src="/separator.svg" />
              <FrameParent5>
                <FrameGroup>
                  <FrameWrapper>
                    <FrameContainer>
                      <AdminPanelSettingsParent>
                        <AdminPanelSettings>
                          <GroupIcon loading="lazy" alt="" />
                          <AdminPanelSettingsChild />
                          <AdminPanelSettingsItem />
                          <AdminPanelSettingsInner />
                          <RectangleDiv />
                        </AdminPanelSettings>
                        <PagetitleWrapper>
                          <Pagetitle5>대시보드</Pagetitle5>
                        </PagetitleWrapper>
                      </AdminPanelSettingsParent>
                      <AdminPanelSettingsGroup>
                        <AdminPanelSettings1
                          loading="lazy"
                          alt=""
                          src="/admin-panel-settings.svg"
                        />
                        <PagetitleWrapper>
                          <Pagetitle6>관리자 관리</Pagetitle6>
                        </PagetitleWrapper>
                      </AdminPanelSettingsGroup>
                    </FrameContainer>
                  </FrameWrapper>
                  <FrameDiv>
                    <ActiveParent>
                      <Active3 />
                      <Active3 />
                    </ActiveParent>
                    <ActiveWrapper>
                      <Active4 />
                    </ActiveWrapper>
                  </FrameDiv>
                </FrameGroup>
                <FrameParent>
                  <FrameWrapper1>
                    <AdminPanelSettingsGroup>
                      <AdminPanelSettings1
                        loading="lazy"
                        alt=""
                        src="/admin-panel-settings-1.svg"
                      />
                      <PagetitleWrapper>
                        <Pagetitle7>칼럼 관리</Pagetitle7>
                      </PagetitleWrapper>
                    </AdminPanelSettingsGroup>
                  </FrameWrapper1>
                  <Active5 />
                </FrameParent>
                <FrameParent>
                  <FrameWrapper1>
                    <AdminPanelSettingsGroup>
                      <AdminPanelSettings1
                        loading="lazy"
                        alt=""
                        src="/admin-panel-settings-2.svg"
                      />
                      <PagetitleWrapper>
                        <Pagetitle8>카테고리 관리</Pagetitle8>
                      </PagetitleWrapper>
                    </AdminPanelSettingsGroup>
                  </FrameWrapper1>
                  <Active5 />
                </FrameParent>
                <FrameParent4>
                  <FrameParent3>
                    <FrameWrapper>
                      <FrameParent1>
                        <BoxParent>
                          <AdminPanelSettings1
                            loading="lazy"
                            alt=""
                            src="/box.svg"
                          />
                          <PagetitleWrapper>
                            <Pagetitle7>상품 관리</Pagetitle7>
                          </PagetitleWrapper>
                        </BoxParent>
                        <AdminPanelSettingsGroup>
                          <AdminPanelSettings1
                            loading="lazy"
                            alt=""
                            src="/admin-panel-settings-3.svg"
                          />
                          <PagetitleWrapper>
                            <Pagetitle6>입점사 관리</Pagetitle6>
                          </PagetitleWrapper>
                        </AdminPanelSettingsGroup>
                      </FrameParent1>
                    </FrameWrapper>
                    <FrameParent2>
                      <ActiveGroup>
                        <Active3 />
                        <Active3 />
                      </ActiveGroup>
                      <AuthenticationWrapper>
                        <Authentication1>
                          <Active />
                        </Authentication1>
                      </AuthenticationWrapper>
                    </FrameParent2>
                  </FrameParent3>
                  <FrameParent>
                    <FrameWrapper1>
                      <AdminPanelSettingsGroup>
                        <GroupIcon1 loading="lazy" alt="" src="/group-1.svg" />
                        <PagetitleWrapper>
                          <Pagetitle7>회원 목록</Pagetitle7>
                        </PagetitleWrapper>
                      </AdminPanelSettingsGroup>
                    </FrameWrapper1>
                    <Active5 />
                  </FrameParent>
                </FrameParent4>
              </FrameParent5>
            </SeparatorParent>
          </SidebarNotificationProC>
        </SidebarNotificationProC1>
        <TagFacesIcon alt="" src="/tag-faces.svg" />
      </Sidebar>
      <Inner>
        <FrameParent11>
          <FrameWrapper2>
            <PagesParent>
              <Pages>Pages / 회원관리</Pages>
              <FrameParent6>
                <Group>
                  <H>관리자 관리</H>
                  <SearchBar2>
                    <Wrapper>
                      <B>관리자 검색</B>
                    </Wrapper>
                    <SearchBarParent>
                      <SearchBar>
                        <Pagetitle9>전체</Pagetitle9>
                        <AdminPanelSettings1
                          alt=""
                          src="/keyboard-arrow-down2.svg"
                        />
                      </SearchBar>
                      <SearchBar1>
                        <AdminPanelSettings1 alt="" src="/search.svg" />
                        <Pagetitle10>이름 / 아이디를 입력하여 검색</Pagetitle10>
                      </SearchBar1>
                      <Button1>
                        <Button>검색</Button>
                      </Button1>
                    </SearchBarParent>
                  </SearchBar2>
                </Group>
                <Button3>
                  <Button2>신규 등록</Button2>
                </Button3>
              </FrameParent6>
            </PagesParent>
          </FrameWrapper2>
          <LargeComplexTableWrapper>
            <LargeComplexTable>
              <Box>
                <Button5>
                  <UnstyledButton>
                    <ButtonText>
                      <Button4>삭제</Button4>
                    </ButtonText>
                  </UnstyledButton>
                </Button5>
              </Box>
              <FrameParent10>
                <TitleWrapper>
                  <Title>
                    <TitleTxt>
                      <B1>3건</B1>
                      <Span>의 검색 결과</Span>
                    </TitleTxt>
                  </Title>
                </TitleWrapper>
                <FrameParent9>
                  <RectangleParent>
                    <FrameChild />
                    <FrameItem />
                    <FrameWrapper10>
                      <FrameParent7>
                        <CheckboxParent>
                          <Checkbox>
                            <Padding>
                              <FilledtogglecheckBoxOutlin type="checkbox" />
                            </Padding>
                          </Checkbox>
                          <FrameWrapper3>
                            <LabelParent>
                              <Label>No</Label>
                              <KeyboardArrowDownIcon
                                alt=""
                                src="/keyboard-arrow-down1.svg"
                              />
                            </LabelParent>
                          </FrameWrapper3>
                        </CheckboxParent>
                        <FrameWrapper4>
                          <LabelParent>
                            <Label1>이름</Label1>
                            <KeyboardArrowDownIcon
                              alt=""
                              src="/keyboard-arrow-down1.svg"
                            />
                          </LabelParent>
                        </FrameWrapper4>
                        <FrameWrapper5>
                          <LabelParent>
                            <Label2>ID</Label2>
                            <KeyboardArrowDownIcon
                              alt=""
                              src="/keyboard-arrow-down1.svg"
                            />
                          </LabelParent>
                        </FrameWrapper5>
                        <FrameWrapper6>
                          <LabelParent>
                            <Label1>부서</Label1>
                            <KeyboardArrowDownIcon
                              alt=""
                              src="/keyboard-arrow-down1.svg"
                            />
                          </LabelParent>
                        </FrameWrapper6>
                        <FrameWrapper7>
                          <LabelParent>
                            <Label3>허용 IP</Label3>
                            <KeyboardArrowDownIcon
                              alt=""
                              src="/keyboard-arrow-down1.svg"
                            />
                          </LabelParent>
                        </FrameWrapper7>
                        <FrameWrapper8>
                          <LabelParent>
                            <Label1>번호</Label1>
                            <KeyboardArrowDownIcon
                              alt=""
                              src="/keyboard-arrow-down1.svg"
                            />
                          </LabelParent>
                        </FrameWrapper8>
                        <FrameWrapper9>
                          <LabelParent>
                            <Label4>마지막 접속일</Label4>
                            <KeyboardArrowDownIcon
                              alt=""
                              src="/keyboard-arrow-down1.svg"
                            />
                          </LabelParent>
                        </FrameWrapper9>
                        <FrameWrapper3>
                          <LabelParent>
                            <Label5>등록 관리자</Label5>
                            <KeyboardArrowDownIcon
                              alt=""
                              src="/keyboard-arrow-down1.svg"
                            />
                          </LabelParent>
                        </FrameWrapper3>
                      </FrameParent7>
                    </FrameWrapper10>
                    <FrameItem />
                  </RectangleParent>
                  <FrameWrapper11>
                    <CheckboxGroup>
                      <Checkbox1>
                        <Padding>
                          <FilledtogglecheckBoxOutlin type="checkbox" />
                        </Padding>
                      </Checkbox1>
                      <Container>
                        <Div>001</Div>
                      </Container>
                      <Frame>
                        <Div1>홍길동</Div1>
                      </Frame>
                      <Dskfjd22Wrapper>
                        <Dskfjd>dskfjd22</Dskfjd>
                      </Dskfjd22Wrapper>
                      <Wrapper1>
                        <Div2>부서명</Div2>
                      </Wrapper1>
                      <Wrapper2>
                        <Div3>--</Div3>
                      </Wrapper2>
                      <Wrapper3>
                        <Div4>00000000001</Div4>
                      </Wrapper3>
                      <Wrapper4>
                        <Div5>2024-01-31</Div5>
                      </Wrapper4>
                      <FrameWrapper3>
                        <Div2>관리자</Div2>
                      </FrameWrapper3>
                    </CheckboxGroup>
                  </FrameWrapper11>
                  <LineDiv />
                  <FrameWrapper11>
                    <CheckboxGroup>
                      <Checkbox1>
                        <Padding>
                          <FilledtogglecheckBox
                            checked={filledtogglecheckBoxIconChecked}
                            type="checkbox"
                            onChange={(event) =>
                              setFilledtogglecheckBoxIconChecked(
                                event.target.checked
                              )
                            }
                          />
                        </Padding>
                      </Checkbox1>
                      <Wrapper5>
                        <Div>002</Div>
                      </Wrapper5>
                      <Wrapper6>
                        <Div1>유재석</Div1>
                      </Wrapper6>
                      <SslsWrapper>
                        <Ssls>393ssls</Ssls>
                      </SslsWrapper>
                      <Wrapper7>
                        <Div2>부서명</Div2>
                      </Wrapper7>
                      <Wrapper8>
                        <Div6>271.0.0.1</Div6>
                      </Wrapper8>
                      <Wrapper9>
                        <Div4>00000000002</Div4>
                      </Wrapper9>
                      <Wrapper10>
                        <Div5>2024-01-24</Div5>
                      </Wrapper10>
                      <Wrapper11>
                        <Div2>관리자</Div2>
                      </Wrapper11>
                    </CheckboxGroup>
                  </FrameWrapper11>
                  <LineDiv />
                  <FrameParent8>
                    <FrameWrapper11>
                      <CheckboxGroup>
                        <Checkbox1>
                          <Padding>
                            <FilledtogglecheckBoxOutlin type="checkbox" />
                          </Padding>
                        </Checkbox1>
                        <Wrapper12>
                          <Div>003</Div>
                        </Wrapper12>
                        <Wrapper13>
                          <Div7>하 하</Div7>
                        </Wrapper13>
                        <Fhdsdv7Wrapper>
                          <Fhdsdv>fhdsdv7</Fhdsdv>
                        </Fhdsdv7Wrapper>
                        <Wrapper14>
                          <Div2>부서명</Div2>
                        </Wrapper14>
                        <Wrapper15>
                          <Div3>--</Div3>
                        </Wrapper15>
                        <Wrapper16>
                          <Div4>00000000003</Div4>
                        </Wrapper16>
                        <Wrapper17>
                          <Div5>2024-01-02</Div5>
                        </Wrapper17>
                        <Wrapper18>
                          <Div2>관리자</Div2>
                        </Wrapper18>
                      </CheckboxGroup>
                    </FrameWrapper11>
                    <FrameChild1 />
                  </FrameParent8>
                </FrameParent9>
              </FrameParent10>
              <LargeComplexTableInner>
                <KeyboardArrowDownParent>
                  <KeyboardArrowDown
                    loading="lazy"
                    alt=""
                    src="/keyboard-arrow-down-9@2x.png"
                  />
                  <Div8>1</Div8>
                  <Div8>2</Div8>
                  <Div8>3</Div8>
                  <Div8>4</Div8>
                  <Div8>5</Div8>
                  <KeyboardArrowDown
                    loading="lazy"
                    alt=""
                    src="/keyboard-arrow-down-10@2x.png"
                  />
                </KeyboardArrowDownParent>
              </LargeComplexTableInner>
            </LargeComplexTable>
          </LargeComplexTableWrapper>
        </FrameParent11>
      </Inner>
    </DivRoot>
  );
};

export default Frame1;
