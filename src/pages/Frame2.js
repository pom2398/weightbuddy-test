import styled from "styled-components";
import FrameComponent4 from "../components/FrameComponent4";
import PathPlanner from "../components/PathPlanner";

const Icon = styled.img`
  height: 2.563rem;
  width: 3.188rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  object-fit: contain;
`;
const VectorIcon = styled.img`
  align-self: stretch;
  height: 1.875rem;
  position: relative;
  max-width: 100%;
  overflow: hidden;
  flex-shrink: 0;
`;
const VectorWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-end;
  padding: 0rem 0rem var(--padding-9xs);
`;
const Parent1 = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-start;
  gap: var(--gap-lgi);
`;
const SidebarNotificationProCInner = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-33xl) 0rem var(--padding-25xl);
  @media screen and (max-width: 450px) {
    padding-left: var(--padding-xl);
    padding-right: var(--padding-xl);
    box-sizing: border-box;
  }
`;
const SeparatorIcon = styled.img`
  align-self: stretch;
  height: 0.063rem;
  position: relative;
  max-width: 100%;
  overflow: hidden;
  flex-shrink: 0;
`;
const GroupIcon = styled.img`
  position: absolute;
  height: 100%;
  width: 100%;
  top: 0%;
  right: 0%;
  bottom: 0%;
  left: 0%;
  max-width: 100%;
  overflow: hidden;
  max-height: 100%;
`;
const AdminPanelSettingsChild = styled.div`
  position: absolute;
  top: 0.313rem;
  left: 0.313rem;
  border-radius: var(--br-12xs);
  background-color: var(--gray-72787f);
  width: 0.438rem;
  height: 0.438rem;
  z-index: 1;
`;
const AdminPanelSettingsItem = styled.div`
  position: absolute;
  top: 0.813rem;
  left: 0.313rem;
  border-radius: var(--br-12xs);
  background-color: var(--gray-72787f);
  width: 0.438rem;
  height: 0.438rem;
  z-index: 1;
`;
const AdminPanelSettingsInner = styled.div`
  position: absolute;
  top: 0.313rem;
  left: 0.813rem;
  border-radius: var(--br-12xs);
  background-color: var(--gray-72787f);
  width: 0.438rem;
  height: 0.438rem;
  z-index: 1;
`;
const RectangleDiv = styled.div`
  position: absolute;
  top: 0.813rem;
  left: 0.813rem;
  border-radius: var(--br-12xs);
  background-color: var(--gray-72787f);
  width: 0.438rem;
  height: 0.438rem;
  z-index: 1;
`;
const AdminPanelSettings = styled.div`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
`;
const Pagetitle = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 3.688rem;
`;
const PagetitleWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-12xs) 0rem 0rem;
`;
const AdminPanelSettingsParent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-lgi) 0rem 0rem;
  gap: var(--gap-xs);
`;
const AdminPanelSettings1 = styled.img`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  min-height: 1.5rem;
`;
const Pagetitle1 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 4.875rem;
`;
const AdminPanelSettingsGroup = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-xs);
`;
const FrameContainer = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-xl);
`;
const FrameWrapper = styled.div`
  width: 7.125rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-7xs) 0rem 0rem;
  box-sizing: border-box;
`;
const Active = styled.div`
  width: 0.25rem;
  height: 2.25rem;
  position: relative;
  border-radius: var(--br-6xl);
`;
const ActiveParent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-5xs);
`;
const Active1 = styled.div`
  width: 0.25rem;
  height: 2.25rem;
  position: relative;
  border-radius: var(--br-6xl);
  background-color: var(--main-0abeba);
`;
const ActiveWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-3xl) 0rem 0rem;
`;
const FrameDiv = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
`;
const FrameGroup = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  padding: 0rem var(--padding-11xs) 0rem 0rem;
  gap: var(--gap-xl);
`;
const Pagetitle2 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 3.938rem;
`;
const FrameWrapper1 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-7xs) 0rem 0rem;
`;
const Active2 = styled.div`
  height: 2.25rem;
  width: 0.25rem;
  position: relative;
  border-radius: var(--br-6xl);
`;
const FrameParent = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  padding: 0rem var(--padding-7xs) 0rem 0rem;
  gap: var(--gap-xl);
`;
const Pagetitle3 = styled.div`
  position: relative;
  font-weight: 500;
  display: inline-block;
  min-width: 5.75rem;
`;
const BoxParent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem var(--padding-mini) 0rem 0rem;
  gap: var(--gap-xs);
`;
const FrameParent1 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-8xl);
`;
const ActiveGroup = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-mini);
`;
const Active3 = styled.div`
  position: absolute;
  top: 0rem;
  left: 0rem;
  border-radius: var(--br-6xl);
  width: 100%;
  height: 100%;
`;
const Authentication = styled.div`
  width: 0.25rem;
  height: 2.25rem;
  position: relative;
`;
const AuthenticationWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-end;
  padding: 0rem 0rem var(--padding-base);
`;
const FrameParent2 = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-start;
  gap: var(--gap-11xs);
`;
const FrameParent3 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  gap: var(--gap-xl);
`;
const GroupIcon1 = styled.img`
  height: 1.5rem;
  width: 1.5rem;
  position: relative;
  min-height: 1.5rem;
`;
const FrameParent4 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  gap: var(--gap-2xs);
`;
const FrameParent5 = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem 0rem 0rem var(--padding-8xl);
  gap: var(--gap-mini);
`;
const SeparatorParent = styled.div`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-start;
  gap: var(--gap-3xl);
`;
const SidebarNotificationProC = styled.div`
  width: 18.125rem;
  background-color: var(--white-ffffff);
  overflow: hidden;
  flex-shrink: 0;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: var(--padding-33xl) 0rem var(--padding-394xl);
  box-sizing: border-box;
  gap: var(--gap-18xl);
  @media screen and (max-width: 1225px) {
    padding-top: var(--padding-15xl);
    padding-bottom: var(--padding-249xl);
    box-sizing: border-box;
  }
  @media screen and (max-width: 450px) {
    gap: var(--gap-lg);
    padding-top: var(--padding-3xl);
    padding-bottom: 10.875rem;
    box-sizing: border-box;
  }
`;
const IterationLoopParent = styled.section`
  align-self: stretch;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-start;
  gap: var(--gap-35xl);
  max-width: 100%;
  @media screen and (max-width: 850px) {
    gap: var(--gap-8xl);
  }
`;
const VariableHolder = styled.main`
  width: 82rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 3.062rem 0rem 0rem;
  box-sizing: border-box;
  max-width: 100%;
  @media screen and (max-width: 1225px) {
    padding-top: var(--padding-13xl);
    box-sizing: border-box;
  }
  @media screen and (max-width: 850px) {
    padding-top: var(--padding-2xl);
    box-sizing: border-box;
  }
`;
const DivRoot = styled.div`
  width: 100%;
  position: relative;
  background-color: var(--gray-f7f8f9);
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 0rem 0rem 5.562rem;
  box-sizing: border-box;
  gap: var(--gap-3xs);
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  font-size: var(--medium-16-size);
  color: var(--gray-72787f);
  font-family: var(--medium-16);
  @media screen and (max-width: 1525px) {
    flex-wrap: wrap;
  }
`;

const Frame2 = () => {
  return (
    <DivRoot>
      <SidebarNotificationProC>
        <SidebarNotificationProCInner>
          <Parent1>
            <Icon loading="lazy" alt="" src="/--1.svg" />
            <VectorWrapper>
              <VectorIcon loading="lazy" alt="" src="/vector.svg" />
            </VectorWrapper>
          </Parent1>
        </SidebarNotificationProCInner>
        <SeparatorParent>
          <SeparatorIcon loading="lazy" alt="" src="/separator.svg" />
          <FrameParent5>
            <FrameGroup>
              <FrameWrapper>
                <FrameContainer>
                  <AdminPanelSettingsParent>
                    <AdminPanelSettings>
                      <GroupIcon loading="lazy" alt="" />
                      <AdminPanelSettingsChild />
                      <AdminPanelSettingsItem />
                      <AdminPanelSettingsInner />
                      <RectangleDiv />
                    </AdminPanelSettings>
                    <PagetitleWrapper>
                      <Pagetitle>대시보드</Pagetitle>
                    </PagetitleWrapper>
                  </AdminPanelSettingsParent>
                  <AdminPanelSettingsGroup>
                    <AdminPanelSettings1
                      loading="lazy"
                      alt=""
                      src="/admin-panel-settings.svg"
                    />
                    <PagetitleWrapper>
                      <Pagetitle1>관리자 관리</Pagetitle1>
                    </PagetitleWrapper>
                  </AdminPanelSettingsGroup>
                </FrameContainer>
              </FrameWrapper>
              <FrameDiv>
                <ActiveParent>
                  <Active />
                  <Active />
                </ActiveParent>
                <ActiveWrapper>
                  <Active1 />
                </ActiveWrapper>
              </FrameDiv>
            </FrameGroup>
            <FrameParent>
              <FrameWrapper1>
                <AdminPanelSettingsGroup>
                  <AdminPanelSettings1
                    loading="lazy"
                    alt=""
                    src="/admin-panel-settings-1.svg"
                  />
                  <PagetitleWrapper>
                    <Pagetitle2>칼럼 관리</Pagetitle2>
                  </PagetitleWrapper>
                </AdminPanelSettingsGroup>
              </FrameWrapper1>
              <Active2 />
            </FrameParent>
            <FrameParent>
              <FrameWrapper1>
                <AdminPanelSettingsGroup>
                  <AdminPanelSettings1
                    loading="lazy"
                    alt=""
                    src="/admin-panel-settings-2.svg"
                  />
                  <PagetitleWrapper>
                    <Pagetitle3>카테고리 관리</Pagetitle3>
                  </PagetitleWrapper>
                </AdminPanelSettingsGroup>
              </FrameWrapper1>
              <Active2 />
            </FrameParent>
            <FrameParent4>
              <FrameParent3>
                <FrameWrapper>
                  <FrameParent1>
                    <BoxParent>
                      <AdminPanelSettings1
                        loading="lazy"
                        alt=""
                        src="/box.svg"
                      />
                      <PagetitleWrapper>
                        <Pagetitle2>상품 관리</Pagetitle2>
                      </PagetitleWrapper>
                    </BoxParent>
                    <AdminPanelSettingsGroup>
                      <AdminPanelSettings1
                        loading="lazy"
                        alt=""
                        src="/admin-panel-settings-3.svg"
                      />
                      <PagetitleWrapper>
                        <Pagetitle1>입점사 관리</Pagetitle1>
                      </PagetitleWrapper>
                    </AdminPanelSettingsGroup>
                  </FrameParent1>
                </FrameWrapper>
                <FrameParent2>
                  <ActiveGroup>
                    <Active />
                    <Active />
                  </ActiveGroup>
                  <AuthenticationWrapper>
                    <Authentication>
                      <Active3 />
                    </Authentication>
                  </AuthenticationWrapper>
                </FrameParent2>
              </FrameParent3>
              <FrameParent>
                <FrameWrapper1>
                  <AdminPanelSettingsGroup>
                    <GroupIcon1 loading="lazy" alt="" src="/group-1.svg" />
                    <PagetitleWrapper>
                      <Pagetitle2>회원 목록</Pagetitle2>
                    </PagetitleWrapper>
                  </AdminPanelSettingsGroup>
                </FrameWrapper1>
                <Active2 />
              </FrameParent>
            </FrameParent4>
          </FrameParent5>
        </SeparatorParent>
      </SidebarNotificationProC>
      <VariableHolder>
        <IterationLoopParent>
          <FrameComponent4 />
          <PathPlanner />
        </IterationLoopParent>
      </VariableHolder>
    </DivRoot>
  );
};

export default Frame2;
